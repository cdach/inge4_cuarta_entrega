<?php
	function fn_mensaje($titulo,$mensaje,$error=false){
		echo '<!-- Modal -->
			<div class="modal fade" id="app-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">' . $titulo . '</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
				  <div class="modal-body">' . $mensaje . '</div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				  </div>
				</div>
			  </div>
			</div>';
		
		echo "<script>
			$('#app-modal').modal('toggle');
		</script>";

		return;
	}
	
	function fn_sesion(){
		@session_start();	
		$valor = isset($_SESSION["usuario"]);
				
		if (!$valor){
			echo "<script> alert('No inició Sesión correctamente...'); 
				location.href='../login/login.php';
			</script>";
		}
	}
	
	function fn_sesion_administrador(){
		@session_start();
		
		$perfil = $_SESSION["perfil"];
				
		if (strcmp($perfil,"A") !== 0){
			echo "<script> alert('No inició Sesión como Administrador...'); 
				location.href='../login/login.php';
			</script>";
		}
	}
		
	function fn_menu(){
		@session_start();
		$perfil = $_SESSION["perfil"];
		
		if($perfil == "A"){
			echo '<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent">
					<span class="navbar-toggle-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Administración
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../usuario/usuario-lista.php">√ Usuarios</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   General
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../taller/taller-lista.php">√ Talleres</a>
								<a class="dropdown-item" href="../horarios/horario-lista.php">√ Horarios</a>
								<a class="dropdown-item" href="../empleado/empleado-lista.php">√ Empleados</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Productos / Servicios
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../unidad/unidad-lista.php">√ Unidades</a>
								<a class="dropdown-item" href="../clasificacion/clasificacion-lista.php"> √ Clasificaciones</a>
								<a class="dropdown-item" href="../marca/marca-lista.php"> √ Marcas</a>
								<a class="dropdown-item" href="../productoservicio/prodserv-lista.php">√ Producto / Servicio</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Clientes
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../cliente/cliente-lista.php">√ Clientes</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Vehiculos
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../vehiculo/vehiculo-lista.php">√ Vehiculos</a>
								<a class="dropdown-item" href="../vehiculosmarcas/vehiculomarca-lista.php">√ Vehiculos Marcas</a>
								<a class="dropdown-item" href="../vehiculosmodelos/vehiculomodelo-lista.php">√ Vehiculos Modelo</a>
							</div>
						</li>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Presupuestos
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../presupuesto/presupuesto-lista.php">Presupuestos de Ventas</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Facturación
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../ventas/venta-lista.php">Facturas</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Web Service
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../webservice/servicio.php?wdsl">Definición</a>
								<a class="dropdown-item" href="../webservice/consultar.php">Consultas</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Informes
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../MyReports/rpt-auditorias.php">√ Auditorías</a>
								<a class="dropdown-item" href="../MyReports/rpt-usuarios.php">√ Usuarios</a>
								<a class="dropdown-item" href="../MyReports/rpt-vehiculos.php">√ Vehículos de Clientes</a>
								<a class="dropdown-item" href="../MyReports/rpt-presupuestos.php">√ Presupuestos</a>
								<a class="dropdown-item" href="../MyReports/rpt-facturaciones.php">√ Facturación</a>
								<a class="dropdown-item" href="../MyReports/rpt-ventas-diarias.php">√ Gráfico de Ventas Diarias</a>
								<a class="dropdown-item" href="../MyReports/rpt-servicios-requeridos.php">√ Gráfico de Servicios Requeridos</a>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav mr-0">
						<li class="nav-item dropdown">
							<a class="nav-link" href="../login/cerrar.php"
							   id="navbarDropdown" data-toggle="">
							   Cerrar Sesión
							</a>
						</li>
					</ul>
				</div>
			</nav>';
		}
		elseif($perfil == "O"){
			echo '<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent">
					<span class="navbar-toggle-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   General
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Empleados</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Productos / Servicios
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../unidad/unidad-lista.php">√ Unidades</a>
								<a class="dropdown-item" href="../clasificacion/clasificacion-lista.php">√ Clasificaciones</a>
								<a class="dropdown-item" href="../marca/marca-lista.php"> √ Marcas</a>
								<a class="dropdown-item" href="../productoservicio/prodserv-lista.php">√ Producto / Servicio</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Clientes
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Clientes</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Vehiculos
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../vehiculo/vehiculo-lista.php">√ Vehiculos</a>
								<a class="dropdown-item" href="../vehiculosmarcas/vehiculomarca-lista.php">√ Vehiculos Marcas</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Presupuestos
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Presupuestos de Ventas</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Facturacion
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Facturas</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Informes
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="../MyReports/rpt-vehiculos.php">√ Vehículos de Clientes</a>
								<a class="dropdown-item" href="../MyReports/rpt-presupuestos.php">√ Presupuestos</a>
								<a class="dropdown-item" href="../MyReports/rpt-facturaciones.php">√ Facturación</a>
								<a class="dropdown-item" href="../MyReports/rpt-ventas-diarias.php">√ Gráfico de Ventas Diarias</a>
								<a class="dropdown-item" href="../MyReports/rpt-servicios-requeridos.php">√ Gráfico de Servicios Requeridos</a>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav mr-0">
						<li class="nav-item dropdown">
							<a class="nav-link" href="../login/cerrar.php
							   id="navbarDropdown" data-toggle="">
							   Cerrar Sesión
							</a>
						</li>
					</ul>
				</div>
			</nav>';
		}
		elseif($perfil == "C"){
			echo '<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent">
					<span class="navbar-toggle-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#"
							   id="navbarDropdown" data-toggle="dropdown">
							   Web Service
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Definicion</a>
								<a class="dropdown-item" href="#">Consultas</a>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav mr-0">
						<li class="nav-item dropdown">
							<a class="nav-link" href="../login/cerrar.php"
							   id="navbarDropdown" data-toggle="">
							   Cerrar Sesión
							</a>
						</li>
					</ul>
				</div>
			</nav>';
		}
	}
	
	function fn_formato_fecha($fecha,$inverso=false){
		if(!$inverso){
			return substr($fecha,8,2) . "/" . substr($fecha,5,2) . "/" . 
			substr($fecha,0,4) . " " . substr($fecha,10,6);
		}
		else{
			return substr($fecha,6,4) . "-" . substr($fecha,3,2) . "-" . 
			substr($fecha,0,2) . " " . substr($fecha,10,6);
		}
	}
	
	function fn_setear_datatable($id){
		echo '<script>
			var table = $("#' . $id . '").DataTable({
				language: {
					"decimal": "",
					"emptyTable": "No hay información",
					"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
					"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
					"infoFiltered": "(Filtrado de _MAX_ total entradas)",
					"infoPostFix": "",
					"thousands": ",",
					"lengthMenu": "Mostrar _MENU_ Entradas",
					"loadingRecords": "Cargando...",
					"processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "Sin resultados encontrados",
					"paginate": {
						"first": "Primero",
						"last": "Ultimo",
						"next": "Siguiente",
						"previous": "Anterior"
					}
				}
			});
		</script>';
	}
	
	function fn_lista_combo($id,$sql,$fk=0,$evento=''){
		include_once("../../clases/Conexion.php");
		$conexion = new Conexion();
	
		if(!isset($fk)) $fk=0;
		
		echo "<select class='form-control' id='$id' name='$id' onchange=$evento>
		<option value=''></option>";

		$resultado = $conexion->consultarSql($sql,true);
				
		if($resultado){
			foreach($resultado as $registro)
			{
				$selected = "";
				if($registro[0] == $fk) 
					$selected = "selected";
				else 
					$seleccionado = "";

				echo "<option value=" . $registro[0] . " $selected>" . $registro[1] . "</option>";
			}
		}
		else
			echo "<option value=''>Sin Registros</option>";
		
		echo "</select>";
		
		unset($conexion);
	}
	
	function fn_validar_dato($dato,$tipo,$formato_fecha='Y-m-d H:i')
	{
		if($tipo == "float"){
			if(filter_var($dato, FILTER_VALIDATE_FLOAT) === false)
				return false;
		}
		else if($tipo == "integer"){
			if(filter_var($dato, FILTER_VALIDATE_INT) === false)
				return false;
		}
		else if($tipo == "string"){
			if(!preg_match('/^[a-zA-Z ñÑáéíóúüç]*$/', $dato))
				return false;
		}
		else if($tipo == "date"){
			$d = DateTime::createFromFormat($formato_fecha, $dato);
			return $d && $d->format($formato_fecha) == $dato;
		}
		else if($tipo == "telefono"){
			if(!preg_match("/^([0-9]){9}|([0-9]){10}|([0-9]){11}|([0-9]){12}|([0-9]){13}|([0-9]){14}
			|([0-9]){15}$/", $dato))
				return false;
		}
		else if($tipo == "email")
		{
			if(filter_var($dato, FILTER_VALIDATE_EMAIL) === false)
				return false;
		}
		else if($tipo == "ruc")
		{
			if(!preg_match('/^([0-9]{8})|([0-9]{7})|([0-9]{6})|([0-9]{6})($-[0-9]{1})$/', $dato))
				return false;
		}
		else if($tipo == "timbrado")
		{
			if(!preg_match('/^[0-9]{8}$/', $dato))
				return false;
		}
		
		return true;
	}
	
	// Digito verificador
	function digito_verificador($numero,$basemax)
	{
		$numero = strtoupper($numero);
		$numero_al = "";
		$k = 2;
		$total = 0; $resto = 0;
		
		for($i=0;$i<strlen($numero);$i++)
		{
			$valor_ascii = ord(substr($numero,$i,1));
			if (!($valor_ascii >= 48 and $valor_ascii <= 57))
				$numero_al = $numero_al . char($valor_ascii);
			else
				$numero_al = $numero_al . substr($numero,$i,1);
		}
				
		for ($i=strlen($numero_al) - 1; $i > -1; $i--)
		{
			if($k > $basemax) $k = 2;
			
			$total = $total + (intval(substr($numero_al,$i,1)) * $k);
			$k++;
		}

		$resto = $total % 11;

		if ($resto > 0)
			return 11 - $resto;
		else
			return 0;
	}
	
	function auditoriaRegistro($t,$a,$o=""){
		include_once("../../clases/Conexion.php");
		$conexion = new Conexion();
		
		@session_start();
		$usuario = $_SESSION['usuario'];
		
		$sentencia = "insert into auditorias(id_usuario,fecha,tabla,accion,descricpcion)
		values('$usuario',current_timestamp,'$t','$a','$o')";
		
		$conexion->consultarSql($sentencia,false);
	}
	
	function escribirLog($tipo)
	{
		if($tipo == "A"){
			$tipo = "ACCESO";
			$prefijo = "→";
		}
		else if($tipo == "S"){
			$tipo = "SALIDA";
			$prefijo = "←";
		}
		
		$cadena = $prefijo . "[" . date("Y-m-d H:i:s") . ". HOST: " . $_SERVER["SERVER_ADDR"] . 
		". AGENTE: " . $_SERVER["HTTP_USER_AGENT"] . ". REQUEST URI: " . $_SERVER['REQUEST_URI'] .
		". USUARIO: " . $_SESSION["usuario"] . ". " . $tipo . "]";
		
		$arch = fopen("../../archivo_log.txt","a+");
		fwrite($arch, $cadena . "\n");
		fclose($arch);
	}
?>
