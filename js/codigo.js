// Función para acceso a la Base de Datos
function cerrar(obj){
	obj = "." + obj;
	$(obj).remove();
}

// Login
function ingresar(){
	var datos={	"usuario":$("#usuario").val(), 
				"clave":$("#clave").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'login-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

// Usuarios
function listarUsuarios(){
	$.ajax({
		type:'post',
		url:'usuario-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoUsuario(){
	var datos={	"id_usuario":$("#id_usuario").val(), 
				"nombre":$("#nombre").val(), 
				"clave":$("#clave").val(),
				"confirmacion":$("#confirmacion").val(),
				"fecha_registro":$("#fecha_registro").val(),
				"estado":$("#estado").val(),
				"perfil":$("#perfil").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'usuario-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarUsuario(){
	var datos={	"id_usuario":$("#id_usuario").val(), 
				"nombre":$("#nombre").val(), 
				"clave":$("#clave").val(),
				"confirmacion":$("#confirmacion").val(),
				"fecha_registro":$("#fecha_registro").val(),
				"estado":$("#estado").val(),
				"perfil":$("#perfil").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'usuario-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarUsuario(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'usuario-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Unidad
function listarUnidad(){
	$.ajax({
		type:'post',
		url:'unidad-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevaUnidad(){
	var datos={	"nombre":$("#nombre").val(), 
				"sigla":$("#sigla").val(),
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'unidad-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarUnidad(){
	var datos={	"id_unidad":$("#id_unidad").val(), 
				"nombre":$("#nombre").val(), 
				"sigla":$("#sigla").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'unidad-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarUnidad(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'unidad-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Taller
function listarTaller(){
	$.ajax({
		type:'post',
		url:'taller-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoTaller(){
	var datos={	"nombre":$("#nombre").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"ruc":$("#ruc").val(),
				"email":$("#email").val(),
				"estado":$("#estado").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'taller-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarTaller(){
	var datos={	"id_taller":$("#id_taller").val(), 
				"nombre":$("#nombre").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"ruc":$("#ruc").val(),
				"email":$("#email").val(),
				"estado":$("#estado").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'taller-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}
function borrarTaller(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'taller-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
// Producto/Servicio
function listarProductoServicio(){
	$.ajax({
		type:'post',
		url:'prodserv-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoProductoServicio(){
	var datos={	"codigo":$("#codigo").val(),
				"nombre":$("#nombre").val(),
				"descripcion":$("#descripcion").val(),
				"id_clasificacion":$("#id_clasificacion").val(),
				"id_unidad":$("#id_unidad").val(),
				"id_marca":$("#id_marca").val(),
				"precio_venta":$("#precio_venta").val(),
				"habilitado":$("#habilitado").val(),
				"iva":$("#iva").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'prodserv-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarProductoServicio(){
	var datos={	"id_servicio_producto":$("#id_servicio_producto").val(), 
				"codigo":$("#codigo").val(), 
				"nombre":$("#nombre").val(),
				"descripcion":$("#descripcion").val(),
				"id_clasificacion":$("#id_clasificacion").val(),
				"id_unidad":$("#id_unidad").val(),
				"id_marca":$("#id_marca").val(),
				"precio_venta":$("#precio_venta").val(),
				"habilitado":$("#habilitado").val(),
				"iva":$("#iva").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'prodserv-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarProductoServicio(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'prodserv-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Clasificacion
function listarClasificacion(){
	$.ajax({
		type:'post',
		url:'clasificacion-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevaClasificacion(){
	var datos={	"nombre":$("#nombre").val(), 
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'clasificacion-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarClasificacion(){
	var datos={	"id_clasificacion":$("#id_clasificacion").val(), 
				"nombre":$("#nombre").val(), 
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'clasificacion-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarClasificacion(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'clasificacion-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Marcas
function listarMarca(){
	$.ajax({
		type:'post',
		url:'marca-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevaMarca(){
	var datos={	"nombre":$("#nombre").val(), 
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'marca-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarMarca(){
	var datos={	"id_marca":$("#id_marca").val(), 
				"nombre":$("#nombre").val(), 
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'marca-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarMarca(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'marca-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Empleados
function listarEmpleados(){
	$.ajax({
		type:'post',
		url:'empleado-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoEmpleado(){
	var datos={	"nombre":$("#nombre").val(), 
				"documento":$("#documento").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"email":$("#email").val(),
				"cargo":$("#cargo").val(),
				"estado":$("#estado").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'empleado-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarEmpleado(){
	var datos={	"id_empleado":$("#id_empleado").val(), 
				"nombre":$("#nombre").val(), 
				"documento":$("#documento").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"email":$("#email").val(),
				"cargo":$("#cargo").val(),
				"estado":$("#estado").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'empleado-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarEmpleado(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'empleado-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
// Clientes
function listarClientes(){
	$.ajax({
		type:'post',
		url:'cliente-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoCliente(){
	var datos={	"nombre":$("#nombre").val(), 
				"documento":$("#documento").val(), 
				"digito":$("#digito").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"email":$("#email").val(),
				"estado":$("#estado").val()
		};
	
	$.ajax({
		data:datos,
		type:'post',
		url:'cliente-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});
}

function editarCliente(){
	var datos={	"id_cliente":$("#id_cliente").val(), 
				"nombre":$("#nombre").val(), 
				"documento":$("#documento").val(), 
				"digito":$("#digito").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"email":$("#email").val(),
				"estado":$("#estado").val()
		};
		
	$.ajax({
		data:datos,
		type:'post',
		url:'cliente-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarCliente(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'cliente-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
// Vehiculos
function listarVehiculos(){
	$.ajax({
		type:'post',
		url:'vehiculo-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoVehiculo(){
	var datos={ "id_marca":$("#id_marca").val(),
				"id_modelo":$("#id_modelo").val(),
				"id_color":$("#id_color").val(),
				"chapa":$("#chapa").val(), 
				"anio":$("#anio").val(),
				"chasis":$("#chasis").val(),
				"id_cliente":$("#id_cliente").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'vehiculo-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarVehiculo(){
	var datos={	"id_vehiculo":$("#id_vehiculo").val(), 
				"id_marca":$("#id_marca").val(), 
				"id_modelo":$("#id_modelo").val(), 
				"id_color":$("#id_color").val(),
				"chapa":$("#chapa").val(), 
				"anio":$("#anio").val(),
				"chasis":$("#chasis").val(),
				"id_cliente":$("#id_cliente").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'vehiculo-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarVehiculo(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'vehiculo-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Horarios
function listarHorarios(){
	$.ajax({
		type:'post',
		url:'horario-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoHorario(){
	var datos={	"id_taller":$("#id_taller").val(), 
				"dia":$("#dia").val(), 
				"desde":$("#desde").val(), 
				"hasta":$("#hasta").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'horario-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarHorario(){
	var datos={	"id_horario":$("#id_horario").val(), 
				"id_taller":$("#id_taller").val(), 
				"dia":$("#dia").val(), 
				"desde":$("#desde").val(), 
				"hasta":$("#hasta").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'horario-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarHorario(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'horario-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

function digitoVerificador(){
	var datos={"documento":$("#documento").val()};
	$.ajax({
		data:datos,
		type:'post',
		url:'../../lib/digito-verificador.php',
		success:function(response){
			$("#digito").val(response);
		}		
	});
}
// Vehiculos Marcas
function listarVehiculosMarcas(){
	$.ajax({
		type:'post',
		url:'vehiculomarca-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoVehiculoMarca(){
	var datos={	"nombre":$("#nombre").val(), 
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'vehiculomarca-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarVehiculoMarca(){
	var datos={	"id_marca":$("#id_marca").val(), 
				"nombre":$("#nombre").val(), 
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'vehiculomarca-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarVehiculoMarca(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'vehiculomarca-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
// Vehiculos Modelo
function listarVehiculosModelos(){
	$.ajax({
		type:'post',
		url:'vehiculomodelo-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoVehiculoModelo(){
	var datos={	"id_marca":$("#id_marca").val(),
				"nombre":$("#nombre").val(), 
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'vehiculomodelo-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarVehiculoModelo(){
	var datos={	"id_modelo":$("#id_modelo").val(), 
				"id_marca":$("#id_marca").val(), 
				"nombre":$("#nombre").val(), 
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'vehiculomodelo-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarVehiculoModelo(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'vehiculomodelo-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
// Color
function listarColores(){
	$.ajax({
		type:'post',
		url:'color-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoColor(){
	var datos={	"nombre":$("#nombre").val(), 
				"codigo":$("#codigo").val(),
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'color-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarColor(){
	var datos={	"id_color":$("#id_color").val(), 
				"nombre":$("#nombre").val(), 
				"codigo":$("#codigo").val(), 
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'color-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarColor(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'color-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Filtros de Datos
// Ruta relativa a vehiculos
function filtrarModelo(){
	var datos={	
			"id_marca":$("#id_marca").val()
		};
		
	$.ajax({
		data:datos,
		type:'post',
		url:'../filtros/filtrar-modelo.php',
		success:function(response){
			$("#id_modelo").html(response);
		}		
	});	
}

function filtrarMarcaCliente(){
	var datos={	
			"id_cliente":$("#id_cliente").val()
		};
	
	$.ajax({
		data:datos,
		type:'post',
		url:'../filtros/filtrar-marca-cliente.php',
		success:function(response){
			$("#id_marca").html(response);
		}		
	});	
}

function verGrafico1(){
	var datos={	
			"desde":$("#desde").val(),
			"hasta":$("#hasta").val(),
			"id_servicio_producto":$("#id_servicio_producto").val()
		};
	
	$.ajax({
		data:datos,
		type:'post',
		url:'rpt-ventas-diarias-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function verGrafico2(){
	var datos={	
			"desde":$("#desde").val(),
			"hasta":$("#hasta").val(),
			"id_servicio_producto":$("#id_servicio_producto").val(),
			"estado":$("#estado").val(),
		};
	
	$.ajax({
		data:datos,
		type:'post',
		url:'rpt-servicios-requeridos-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

// Producto/Servicio
function listarPresupuestos(){
	$.ajax({
		type:'post',
		url:'presupuesto-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoPresupuesto(){
	var datos={	"id_taller":$("#id_taller").val(),
				"id_clientee":$("#id_cliente").val(),
				"id_vehiculo":$("#id_vehiculo").val(),
				"fecha":$("#fecha").val(),
				"fecha_validacion":$("#fecha_validacion").val(),
				"presupuestado_por":$("#presupuestado_por").val(),
				"total":$("#total").val(),
				"condicion":$("#condicion").val(),
				"observacion":$("#observacion").val(),
				"estado":$("#estado").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'presupuesto-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarPresupuesto(){
	var datos={	"id_presupuesto":$("#id_presupuesto").val(),
				"id_taller":$("#id_taller").val(),
				"id_clientee":$("#id_cliente").val(),
				"id_vehiculo":$("#id_vehiculo").val(),
				"fecha":$("#fecha").val(),
				"fecha_validacion":$("#fecha_validacion").val(),
				"presupuestado_por":$("#presupuestado_por").val(),
				"total":$("#total").val(),
				"condicion":$("#condicion").val(),
				"observacion":$("#observacion").val(),
				"estado":$("#estado").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'presupuesto-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarPresupuesto(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'presupuesto-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Ventas
function listarVentas(){
	$.ajax({
		type:'post',
		url:'venta-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevaVenta(){
	var datos={	"id_taller":$("#id_taller").val(),
				"numero_factura":$("#numero_factura").val(),
				"id_presupuesto":$("#id_presupuesto").val(),
				"id_cliente":$("#id_cliente").val(),
				"fecha":$("#fecha").val(),
				"facturado_por":$("#facturado_por").val(),
				"total":$("#total").val(),
				"observacion":$("#observacion").val(),
				"estado":$("#estado").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'venta-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarVenta(){
	var datos={	"id_venta":$("#id_venta").val(), 
				"id_taller":$("#id_taller").val(), 
				"numero_factura":$("#numero_factura").val(),
				"id_presupuesto":$("#id_presupuesto").val(),
				"id_cliente":$("#id_cliente").val(),
				"fecha":$("#fecha").val(),
				"facturado_por":$("#facturado_por").val(),
				"total":$("#total").val(),
				"condicion":$("#condicion").val(),
				"observacion":$("#observacion").val(),
				"estado":$("#estado").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'venta-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarVenta(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'venta-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
function llamarServicio(){
	var datos={	
			"id_taller":$("#id_taller").val(),
			"dia":$("#dia").val(),
		};
	
	$.ajax({
		data:datos,
		type:'post',
		url:'consultar-procesar.php',
		beforeSend:function(){
			$("#rs-ajax").html("Procesando Respuesta...");
		},		
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}