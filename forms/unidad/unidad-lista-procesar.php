<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Unidad.php");
	
	$unidad = new Unidad();
	$rs = $unidad->listarUnidad();
	
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Sigla</th>
                <th>Unidad</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila){
			
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["sigla"] . "</td>
					<td>" . $fila["id_unidad"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='unidad-editar.php?id=" . $fila["id_unidad"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarUnidad(" . '"' . $fila["id_unidad"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>	
					</td>
				 </tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($unidad);
?>