<?php
	include_once("../../lib/funciones.php");
	
	$nombre 		= $_POST["nombre"];
	$sigla 			= $_POST["sigla"];
	
	include_once("unidad-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Unidad.php");
	$unidad = new Unidad("",$nombre,$sigla);
		
	if($unidad->grabarUnidad()){
		unset($unidad);
		echo "<script> location.href='unidad-lista.php';</script>";
	}
?>