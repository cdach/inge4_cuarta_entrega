<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Taller.php");
	
	$taller = new Taller();
	$rs = $taller->listarTalleres();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Direccion</th>
				<th>Localidad</th>
                <th>Estado</th>
                <th>Taller</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
			$estado = (($fila["estado"] === "A") ? "Activo" : "Inactivo");
				
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["direccion"] . "</td>
					<td>" . $fila["localidad"] . "</td>
					<td>" . $estado . "</td>
					<td>" . $fila["id_taller"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='taller-editar.php?id=" . $fila["id_taller"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarTaller(" . '"' . $fila["id_taller"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($taller);