<?php
	$controlError = true;
	
	echo "<script>
			$('#nombre_ayuda').html('');
			$('#direccion_ayuda').html('');
			$('#localidad_ayuda').html('');
			$('#telefono_principal_ayuda').html('');
			$('#telefono_secundario_ayuda').html('');
			$('#ruc_ayuda').html('');
			$('#email_ayuda').html('');
			$('#estado_ayuda').html('');
		</script>";
		
	if($nombre == ""){
		echo "<script> $('#nombre_ayuda').html('Debe ingresar el nombre!!!'); 
		$('#nombre').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($direccion == ""){
		echo "<script> $('#direccion_ayuda').html('Debe ingresar la direccion!!!'); 
		$('#direccion').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($localidad == ""){
		echo "<script> $('#localidad_ayuda').html('Debe ingresar la localidad!!!'); 
		$('#localidad').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if(!fn_validar_dato($telefono_principal,"telefono")){
		echo "<script> $('#telefono_principal_ayuda').html('Debe ingresar un teléfono principal en el formato requerido!!!'); 
		$('#telefono_principal').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($telefono_secundario !="" && !fn_validar_dato($telefono_secundario,"telefono"))
	{
		echo "<script> $('#telefono_secundario_ayuda').html('Debe ingresar un teléfono válido!!!'); 
		$('#telefono_secundario').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if(!fn_validar_dato($ruc,"ruc")))
	{
		echo "<script> $('#ruc_ayuda').html('Debe ingresar un ruc válido!!!'); 
		$('#ruc').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if(!fn_validar_dato($email,"email"))
	{
		echo "<script> $('#email_ayuda').html('Debe ingresar un email válido!!!'); 
		$('#email').focus(); </script>";
		$controlError = false;
		return;
	}
?>