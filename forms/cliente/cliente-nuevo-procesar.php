<?php
	include_once("../../lib/funciones.php");
	
	$nombre 			= $_POST["nombre"];
	$documento 			= $_POST["documento"];
	$digito				= $_POST["digito"];
	$direccion 			= $_POST["direccion"];
	$localidad 			= $_POST["localidad"];
	$telefono_principal = $_POST["telefono_principal"];
	$telefono_secundario = $_POST["telefono_secundario"];
	$email 				= $_POST["email"];
	$estado 			= $_POST["estado"];
		
	include_once("cliente-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Cliente.php");
	$cliente = new Cliente("",$nombre,$documento,$digito,$direccion,$localidad,
	$telefono_principal,$telefono_secundario,$email,$estado);
		
	if($cliente->grabarCliente()){
		auditoriaRegistro('CLIENTES','G','Cliente agregado: ' . $nombre);
		unset($cliente);
		echo "<script> location.href='cliente-lista.php';</script>";
	}
?>