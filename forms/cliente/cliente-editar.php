<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
			
			include_once("../../clases/Cliente.php");
			$id = $_GET["id"];
			$cliente= new Cliente();
			$cliente->recuperarCliente($id);
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Editar Cliente</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Cliente</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="id_cliente" name="id_cliente" 
						placeholder="ID de Cliente" maxlength="20" value="<?php echo $cliente->getIdCliente(); ?>"
						readonly>
						<small id="id_cliente_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre y Apellido del Cliente" maxlength="50" value="<?php echo $cliente->getNombre(); ?>">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Documento / RUC</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="documento" name="documento"
						placeholder="Ej.: 4123789" maxlength="10" onchange="$('#digito').val('');" value="<?php echo $cliente->getDocumento(); ?>">
						<small id="documento" class="form-text text-muted"></small>
					</div>
					<div class="form-group">
						<label for="">
							<button type="button" class="btn btn-outline-primary btn-sm" onclick="digitoVerificador();">Dígito</button>
							<button type="button" class="btn btn-outline-primary btn-sm" onclick="$('#digito').val('');">Limpiar</button>
						</label>
						<div class="col-xs-1">
							<input type="text" class="form-control" id="digito" name="digito"
							placeholder="Ej.: 1" maxlength="1" value="<?php echo $cliente->getDigito(); ?>">
							<small id="digito" class="form-text text-muted"></small>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="">Dirección</label>
					<div class="col-xs-1">
						<textarea class="form-control" id="direccion" name="direccion"
						placeholder="Dirección" rows="2"><?php echo $cliente->getDireccion(); ?></textarea>
						<small id="direccion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Localidad</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="localidad" name="localidad"
						placeholder="Localidad" maxlength="50" value="<?php echo $cliente->getLocalidad(); ?>">
						<small id="localidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Teléfono Principal</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_principal" name="telefono_principal"
						placeholder="Telefono Principal" maxlength="15" value="<?php echo $cliente->getTelefonoPrincipal(); ?>">
						<small id="telefono_principal_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Teléfono Secundario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_secundario" name="telefono_secundario"
						placeholder="Telefono Secundario" maxlength="15" value="<?php echo $cliente->getTelefonoSecundario(); ?>">
						<small id="telefono_secundario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<div class="col-xs-1">
						<input type="email" class="form-control" id="email" name="email"
						placeholder="Ej.: micorreo@dominio.com" maxlength="50" value="<?php echo $cliente->getEmail(); ?>">
						<small id="email_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" name="estado" class="form-control">
							<option value="A"<?php echo 'A' === $cliente->getEstado() ? "selected='selected'" : ""; ?> >Activo</option>
							<option value="I"<?php echo 'I' === $cliente->getEstado() ? "selected='selected'" : ""; ?> >Inactivo</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>			
				<br>
				<button type="button" class="btn btn-primary" onclick="editarCliente();">Editar</button>
				<button type="button" class="btn btn-success" onclick="location.href='cliente-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#estado").select2();
		$("#nombre").focus();
	</script>
</html>