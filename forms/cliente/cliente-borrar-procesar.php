<?php
	include_once("../../clases/Cliente.php");
	
	$id = $_POST["id"];
	$cliente = new Cliente();
		
	if($cliente->borrarCliente($id)){
		auditoriaRegistro('CLIENTES','B','Cliente Borrado: ' . $id);
		unset($cliente);
		echo "<script> listarClientes(); </script>";
	}
?>