<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Cliente.php");
	
	$cliente = new Cliente();
	$rs = $cliente->listarClientes();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Documento</th>
                <th>Dirección</th>
                <th>Estado</th>
                <th>ID</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
			
			$estado = (($fila["estado"] === "A") ? "Activo" : "Inactivo");
			$documento = (($fila["digito"] === " ") ? $fila["documento"] : $fila["documento"] . '-' . $fila["digito"]);
			
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $documento . "</td>
					<td>" . $fila["direccion"] . "</td>
					<td>" . $estado . "</td>
					<td>" . $fila["id_cliente"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='cliente-editar.php?id=" . $fila["id_cliente"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarCliente(" . '"' . $fila["id_cliente"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($cliente);