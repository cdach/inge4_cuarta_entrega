<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Cliente</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre y Apellido del Cliente" maxlength="50">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Documento / RUC</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="documento" name="documento"
						placeholder="Ej.: 4123789" maxlength="10" onchange="$('#digito').val('');">
						<small id="documento_ayuda" class="form-text text-muted"></small>
					</div>
					<label for="">
						<button type="button" class="btn btn-outline-primary btn-sm" onclick="digitoVerificador();">Dígito</button>
						<button type="button" class="btn btn-outline-primary btn-sm" onclick="$('#digito').val('');">Limpiar</button>
					</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="digito" name="digito"
						placeholder="Ej.: 1" maxlength="1" disabled>
						<small id="digito_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Dirección</label>
					<div class="col-xs-1">
						<textarea class="form-control" id="direccion" name="direccion"
						placeholder="Dirección" rows="2"></textarea>
						<small id="direccion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Localidad</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="localidad" name="localidad"
						placeholder="Ej.: Mi Localidad" maxlength="50">
						<small id="localidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Teléfono Principal</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_principal" name="telefono_principal"
						placeholder="Ej.: 021595636" maxlength="15">
						<small id="telefono_principal_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Teléfono Secundario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_secundario" name="telefono_secundario"
						placeholder="Ej.: 0981662323" maxlength="15">
						<small id="telefono_secundario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<div class="col-xs-1">
						<input type="email" class="form-control" id="email" name="email"
						placeholder="Ej.: micorreo@dominio.com" maxlength="50">
						<small id="email_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" class="form-control">
							<option value="I">Inactivo</option>
							<option value="A" selected>Activo</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>			
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoCliente();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='cliente-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#estado").select2();
		$("#nombre").focus();
	</script>
</html>