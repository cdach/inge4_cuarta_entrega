<?php
	include_once("../../lib/funciones.php");
	
	$id_taller 			= $_POST["id_taller"];
	$id_cliente 		= $_POST["id_cliente"];
	$id_vehiculo		= $_POST["id_vehiculo"];
	$fecha				= $_POST["fecha"];
	$fecha_validacion	= $_POST["fecha_validacion"];
	$presupuestado_por 	= $_POST["presupuestado_por"];
	$total 				= $_POST["total"];
	$condicion 			= $_POST["condicion"];
	$observacion 		= $_POST["observacion"];
	$estado 			= $_POST["estado"];
	
	include_once("presupuesto-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Presupuesto.php");
		
	$presupuesto = new Presupuesto("",$id_taller,$id_cliente,$id_vehiculo,
		$fecha,$fecha_validacion,$presupuestado_por,$total,$condicion,$observacion,$estado);
		
	if($presupuesto->grabarPresupuesto()){
		auditoriaRegistro('PRESUPUESTOS','G','Presupuesto agregado: ' . $presupuestado_por);
		unset($presupuesto);
		echo "<script> location.href='presupuesto-lista.php';</script>";
	}
?>