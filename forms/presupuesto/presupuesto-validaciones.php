<?php
	$controlError = true;
	
	echo "<script>
			$('#id_taller_ayuda').html('');
			$('#id_cliente_ayuda').html('');
			$('#id_vehiculo_ayuda').html('');
			$('#fecha_ayuda').html('');
			$('#fecha_validacion_ayuda').html('');
			$('#presupuestado_por_ayuda').html('');
			$('#total_ayuda').html('');
			$('#condicion_ayuda').html('');
			$('#observacion_ayuda').html('');
			$('#estado_ayuda').html('');
		 </script>";
			
	if($id_taller == ""){
		echo "<script> $('#id_taller_ayuda').html('Debe ingresar el taller!!!'); 
		$('#id_taller').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($id_cliente == ""){
		echo "<script> $('#id_cliente_ayuda').html('Debe ingresar el cliente!!!'); 
		$('#id_cliente').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($id_vehiculo == ""){
		echo "<script> $('#id_vehiculo_ayuda').html('Debe ingresar el vehiculo!!!'); 
		$('#id_vehiculo').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($fecha == ""){
		echo "<script> $('#fecha_ayuda').html('Debe ingresar la fecha!!!'); 
		$('#fecha').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($fecha_validacion == ""){
		echo "<script> $('#fecha_validacion_ayuda').html('Debe ingresar la fecha de validacion!!!'); 
		$('#fecha_validacion').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if(!fn_validar_dato($total,"total") || $total < 0){
		echo "<script> $('#total_ayuda').html('Debe ingresar un precio de venta correcto!!!'); 
		$('#total').focus(); </script>";
		$controlError = false;
		return;
	}
?>