<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Presupuesto</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Presupuesto</label>
					<div class="col-xs-1">
						<input type="int" class="form-control" id="id_presepuesto" name="id_presepuesto" 
						placeholder="ID de Presupuesto" maxlength="20" value="<?php echo $producto->getIdServicioProducto(); ?>"
						readonly>
						<small id="id_servicio_producto_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_taller","select id_taller,nombre from talleres order by 2",
						$presupuesto->getIdTaller(),""); ?>
						<small id="id_taller_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cliente</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_cliente","select id_cliente, nombre from clientes order by 2",
						$presupuesto->getIdCliente(),""); ?>
						<small id="id_cliente_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Vehiculo</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_vehiculo","select id_vehiculo, nombre from vehiculos order by 2",
						$presupuesto->getIdVehiculo(),""); ?>
						<small id="id_vehiculo_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Fecha</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="fecha" name="fecha"
						placeholder="Fecha" rows="5" value="<?php echo $horario->getFecha(); ?>"/>
						<small id="desde_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Fecha Validacion</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="fecha" name="fecha"
						placeholder="Fecha" rows="5" value="<?php echo $horario->getFechaValidacion(); ?>"/>
						<small id="desde_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Presupuestado por</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_empleado","select id_empleado, nombre from empleados order by 2",
						$presupuesto->getPresupuestadoPor(),""); ?>
						<small id="id_presupuestado_por_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Total</label>
					<div class="col-xs-1">
						<input type="number" class="form-control" id="total" name="total"
						placeholder="Entre 0 y 999.999.999" maxlength="12" 
						value="<?php echo $presupuesto->getTotal(); ?>">
						<small id="total_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				
				<div class="form-group">
					<label for="">Condicion</label>
					<div class="col-xs-1">
						<select id="iva" name="iva" class="form-control">
							<option value="C"<?php echo 'C' === $producto->getCondicion() ? "selected='selected'" : ""; ?> >Contado</option>
							<option value="R"<?php echo 'R' === $producto->getCondicion() ? "selected='selected'" : ""; ?> >Credito</option>
						</select>
						<small id="iva_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Observacion</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="observacion" name="observacion"
						placeholder="Observacion" maxlength="50" 
						value="<?php echo $producto->getObservacion(); ?>">
						<small id="observacion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="iva" name="condicion" class="form-control">
							<option value="A"<?php echo 'A' === $producto->getEstado() ? "selected='selected'" : ""; ?> >Aprobado</option>
							<option value="P"<?php echo 'P' === $producto->getEstado() ? "selected='selected'" : ""; ?> >Pendiente</option>
							<option value="R"<?php echo 'R' === $producto->getEstado() ? "selected='selected'" : ""; ?> >Cancelado</option>
						</select>
						<small id="condicion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoPresupuesto();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='prodserv-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		jQuery("#fecha").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		jQuery("#fecha_validacion").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		
		$("#id_taller").select2();
		$("#id_cliente").select2();
		$("#id_vehiculo").select2();
		$("#id_empleado").select2();
		$("#condicion").select2();
		$("#estado").select2();
		$("#id_taller").focus();
	</script>
</html>