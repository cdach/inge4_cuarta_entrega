<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="../../js/datetimepicker/jquery.datetimepicker.css">
		<script src="../../js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Presupuesto</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_taller","select id_taller,nombre from talleres order by 2",
						0,""); ?>
						<small id="id_taller_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cliente</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_cliente","select id_cliente, nombre from clientes order by 2",0,""); ?>
						<small id="id_cliente_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Vehiculo</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_vehiculo","select ve.id_vehiculo, mo.nombre from vehiculos as ve, vehiculos_modelos as mo where ve.id_modelo = mo.id_modelo order by 2",0,""); ?>
						<small id="id_vehiculo_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Fecha</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="fecha" name="fecha"
						maxlength="12">
						<small id="fecha_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Fecha de Validez</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="fecha_validacion" 
						name="fecha_validacion" placeholder="dd/mm/aaaa hh:mm" maxlength="16" value=""> 
					</div>
				</div>
				<div class="form-group">
					<label for="">Presupuestado por</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_empleado","select id_empleado, nombre from empleados order by 2",0,""); ?>
						<small id="id_presupuestado_por_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Total</label>
					<div class="col-xs-1">
						<input type="number" class="form-control" id="total" name="total"
						placeholder="Entre 0 y 999.999.999" maxlength="12" value="0">
						<small id="total_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				
				<div class="form-group">
					<label for="">Condicion</label>
					<div class="col-xs-1">
						<select id="condicion" name="condicion" class="form-control">
							<option value="C" selected>Contado</option>
							<option value="R" >Credito</option>
						</select>
						<small id="condicion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Observacion</label>
					<div class="col-xs-1">
						<input type="observacion" class="form-control" id="observacion" name="observacion"
						placeholder="Observacion" maxlength="50">
						<small id="observacion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" name="estado" class="form-control">
							<option value="A" selected>Aprobado</option>
							<option value="P" >Pendiente</option>
							<option value="R" >Rechazado</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoPresupuesto();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='prodserv-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		jQuery("#fecha").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		jQuery("#fecha_validacion").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		
		$("#id_taller").select2();
		$("#id_cliente").select2();
		$("#id_vehiculo").select2();
		$("#id_empleado").select2();
		$("#condicion").select2();
		$("#estado").select2();
		$("#id_taller").focus();
	</script>
</html>