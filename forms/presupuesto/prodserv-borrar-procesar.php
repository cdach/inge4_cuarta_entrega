<?php
	include_once("../../clases/ProductoServicio.php");
	
	$id = $_POST["id"];
	$producto = new ProductoServicio();
	
	
	if($producto->borrarProductoServicio($id)){
		auditoriaRegistro('USUARIOS','G','Producto/Servicio Eliminado: ' . $id);
		unset($producto);
		echo "<script> listarProductoServicio(); </script>";
	}
?>