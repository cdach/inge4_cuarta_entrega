<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Presupuesto.php");
	
	$presupuesto = new Presupuesto();
	$rs = $presupuesto->listarPresupuestos();
	
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Chapa</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Total Vta.</th>
                <th>Condicion</th>
                <th>Estado</th>
                <th>ID</th>
                <th>Acciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila){
			
			echo "<tr>
					<td>" . $fila["fecha"] . "</td>
					<td>" . $fila["nombre_cliente"] . "</td>
					<td>" . $fila["chapa"] . "</td>
					<td>" . $fila["nombre_marca"] . "</td>
					<td>" . $fila["nombre_modelo"] . "</td>
					<td>" . $fila["total"] . "</td>
					<td>" . $fila["condicion"] . "</td>
					<td>" . $fila["estado"] . "</td>
					<td>" . $fila["id"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='presupuesto-editar.php?id=" . $fila["id"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarPresupuesto(" . '"' . $fila["id"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>	
					</td>
				 </tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($unidad);