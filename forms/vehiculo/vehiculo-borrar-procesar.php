<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Vehiculo.php");
	
	$id = $_POST["id"];
	$vehiculo = new Vehiculo();
	
	
	if($vehiculo->borrarVehiculo($id)){
		auditoriaRegistro('VEHICULOS','B','Vehiculo Borrado: ' . $id);
		unset($vehiculo);
		echo "<script> listarVehiculos(); </script>";
	}
?>