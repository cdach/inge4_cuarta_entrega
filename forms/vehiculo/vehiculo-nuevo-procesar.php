<?php
	include_once("../../lib/funciones.php");
	
	$id_marca 	= $_POST["id_marca"];
	$id_modelo 	= $_POST["id_modelo"];
	$id_color 	= $_POST["id_color"];
	$chapa 		= $_POST["chapa"];
	$anio 		= $_POST["anio"];
	$chasis 	= $_POST["chasis"];
	$id_cliente = $_POST["id_cliente"];
	
	include_once("vehiculo-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Vehiculo.php");
	$vehiculo = new Vehiculo("",$id_marca,$id_modelo,$id_color,$chapa,$anio,$chasis,$id_cliente);
		
	if($vehiculo->grabarVehiculo()){
		auditoriaRegistro('VEHICULOS','G','Vehículo agregado: ' . $chapa);
		unset($vehiculo);
		echo "<script> location.href='vehiculo-lista.php';</script>";
	}
?>