<?php
	$controlError = true;
	
	echo "<script>
			$('#id_marca_ayuda').html('');
			$('#id_modelo_ayuda').html('');
			$('#id_color_ayuda').html('');
			$('#chapa_ayuda').html('');
			$('#anio_ayuda').html('');
			$('#chasis_ayuda').html('');
			$('#id_cliente_ayuda').html('');
		</script>";
		
	
	if($id_marca == ""){
		echo "<script> $('#id_marca_ayuda').html('Debe ingresar la marca!!!'); 
		$('#id_marca').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($id_modelo == ""){
		echo "<script> $('#id_modelo_ayuda').html('Debe ingresar el modelo!!!'); 
		$('#id_modelo').focus(); </script>";
		$controlError = false;
		return;
	}	
	if($id_color == ""){
		echo "<script> $('#id_color_ayuda').html('Debe ingresar el color!!!'); 
		$('#id_color').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($chapa == ""){
		echo "<script> $('#chapa_ayuda').html('Debe ingresar la chapa!!!'); 
		$('#chapa').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($anio == "" || ($anio < 1960 || $anio > 2050) || !fn_validar_dato($anio,"integer")){
		echo "<script> $('#anio_ayuda').html('Debe un año entre 1960 y 2050!!!'); 
		$('#anio').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($chasis == ""){
		echo "<script> $('#chasis_ayuda').html('Debe ingresar el chasis!!!'); 
		$('#chasis').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($id_cliente == ""){
		echo "<script> $('#id_cliente_ayuda').html('Debe ingresar el cliente!!!'); 
		$('#id_cliente').focus(); </script>";
		$controlError = false;
		return;
	}
?>