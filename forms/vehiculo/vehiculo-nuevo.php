<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Vehículo</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Marca</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_marca","select id_marca,nombre from vehiculos_marcas order by 2",
						0,"filtrarModelo();"); ?>
						<small id="id_marca_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Modelo</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_modelo","select id_modelo,nombre from vehiculos_modelos order by 2",
						0,""); ?>
						<small id="id_modelo_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Color</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_color","select id_color,nombre from vehiculos_colores order by 2",
						0,""); ?>
						<small id="id_color_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Chapa</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="chapa" name="chapa"
						placeholder="Chapa" maxlength="15">
						<small id="chapa_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				<div class="form-group">
					<label for="">Año</label>
					<div class="col-xs-1">
						<input type="number" class="form-control" id="anio" name="anio"
						placeholder="Año" min="0" max="2050" value="">
						<small id="anio_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Chasis</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="chasis" name="chasis"
						placeholder="Chasis" maxlength="50">
						<small id="chasis_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cliente</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_cliente","select id_cliente,nombre from clientes order by 2",
						0,""); ?>
						<small id="id_cliente_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoVehiculo();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='vehiculo-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#id_marca").select2();
		$("#id_modelo").select2();
		$("#id_color").select2();
		$("#id_cliente").select2();
		$("#chapa").focus();
	</script>
</html>