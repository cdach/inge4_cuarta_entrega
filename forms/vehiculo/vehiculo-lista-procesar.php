<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Vehiculo.php");
	
	$vehiculo = new Vehiculo();
	$rs = $vehiculo->listarVehiculos();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Chapa</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Color</th>
				<th>Año</th>
				<th>Chasis</th>
				<th>Cliente</th>
				<th>ID</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
			$nombre_cliente = ((is_null($fila["nombre_cliente"]) || $fila["nombre_cliente"] === "") ? 
			"[Ingrese Cliente]" : $fila["nombre_cliente"]);
			echo "<tr>
					<td>" . $fila["chapa"] . "</td>
					<td>" . $fila["nombre_marca"] . "</td>
					<td>" . $fila["nombre_modelo"] . "</td>
					<td>" . $fila["nombre_color"] . "</td>
					<td>" . $fila["anio"] . "</td>
					<td>" . $fila["chasis"] . "</td>
					<td>" . $nombre_cliente . "</td>
					<td>" . $fila["id_vehiculo"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='vehiculo-editar.php?id=" . $fila["id_vehiculo"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarVehiculo(" . '"' . $fila["id_vehiculo"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($vehiculo);