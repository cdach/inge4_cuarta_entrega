<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../js/bootstrap/js/popper.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- Datatables -->
		<link rel="stylesheet" type="text/css" href="../../js/bootstrap/css/jquery.dataTables.min.css">
		<script src="../../js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Vehículos</h1>
			<div id="rs-ajax"></div>
			<div id="rs-borrar"></div>
			<button type="button" class="btn btn-primary" onclick="location.href='vehiculo-nuevo.php'">Nuevo (+)</button>
		</div>
	</body>
	<script>
		listarVehiculos();
	</script>
</html>