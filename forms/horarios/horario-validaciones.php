<?php
	$controlError = true;
	
	echo "<script>
			$('#id_taller_ayuda').html('');
			$('#dia_ayuda').html('');
			$('#desde_ayuda').html('');
			$('#hasta_ayuda').html('');
		</script>";
		
	if($id_taller == ""){
		echo "<script> $('#id_taller_ayuda').html('Debe ingresar el taller!!!'); 
		$('#id_taller').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($dia == ""){
		echo "<script> $('#dia_ayuda').html('Debe ingresar el dia!!!'); 
		$('#dia').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($desde == ""){
		echo "<script> $('#desde_ayuda').html('Debe ingresar la hora desde!!!'); 
		$('#desde').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($hasta == "" || $desde > $hasta){
		echo "<script> $('#hasta_ayuda').html('Debe ingresar una hora hasta válida!!!'); 
		$('#hasta').focus(); </script>";
		$controlError = false;
		return;
	}
?>