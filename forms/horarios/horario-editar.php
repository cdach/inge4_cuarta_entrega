<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
			
			include_once("../../clases/Horario.php");
			$id = $_GET["id"];
			$horario= new Horario();
			$horario->recuperarHorario($id);
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Editar Horario</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Horario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="id_horario" name="id_horario" 
						placeholder="ID de Horario" maxlength="20" value="<?php echo $horario->getIdHorario(); ?>"
						readonly>
						<small id="id_horario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_taller","select id_taller, nombre from talleres order by 2",
						$horario->getIdTaller(),""); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="">Dia</label>
					<div class="col-xs-1">
						<select id="dia" name="dia" class="form-control">
							<option value="1"<?php echo '1' === $horario->getDia() ? "selected='selected'" : ""; ?> >Domingo</option>
							<option value="2"<?php echo '2' === $horario->getDia() ? "selected='selected'" : ""; ?> >Lunes</option>
							<option value="3"<?php echo '3' === $horario->getDia() ? "selected='selected'" : ""; ?> >Martes</option>
							<option value="4"<?php echo '4' === $horario->getDia() ? "selected='selected'" : ""; ?> >Miércoles</option>
							<option value="5"<?php echo '5' === $horario->getDia() ? "selected='selected'" : ""; ?> >Jueves</option>
							<option value="6"<?php echo '6' === $horario->getDia() ? "selected='selected'" : ""; ?> >Viernes</option>
							<option value="7"<?php echo '7' === $horario->getDia() ? "selected='selected'" : ""; ?> >Sábado</option>
                        </select>
						<small id="dia" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Desde</label>
					<div class="col-xs-1">
						<input type="time" class="form-control" id="desde" name="desde"
						placeholder="Desde" rows="5" value="<?php echo substr($horario->getDesde(),0,5); ?>"/>
						<small id="desde_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Hasta</label>
					<div class="col-xs-1">
						<input type="time" class="form-control" id="hasta" name="hasta"
						placeholder="Hasta" rows="5" value="<?php echo substr($horario->getHasta(),0,5); ?>"/>
						<small id="hasta_ayuda" class="form-text text-muted"></small>
					</div>
				</div>			
				<br>
				<button type="button" class="btn btn-primary" onclick="editarHorario();">Editar</button>
				<button type="button" class="btn btn-success" onclick="location.href='horario-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#id_taller").select2();
		$("#dia").select2();
		$("#id_taller").focus();
	</script>
</html>