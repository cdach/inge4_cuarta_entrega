<?php
	include_once("../../lib/funciones.php");
	
	$id_taller 		= $_POST["id_taller"];
	$dia 			= $_POST["dia"];
	$desde 			= $_POST["desde"];
	$hasta 			= $_POST["hasta"];
	
	include_once("horario-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Horario.php");
	$horario = new Horario("",$id_taller,$dia,$desde,$hasta);
		
	if($horario->grabarHorario()){
		unset($horario);
		echo "<script> location.href='horario-lista.php';</script>";
	}
?>