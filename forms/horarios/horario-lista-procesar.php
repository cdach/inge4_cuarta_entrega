<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Horario.php");
	
	$horario = new Horario();
	$rs = $horario->listarHorarios();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Taller</th>
                <th>Día</th>
                <th>Desde</th>
				<th>Hasta</th>
				<th>ID</th>
				<th>Acciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["nombre_dia"] . "</td>
					<td>" . substr($fila["desde"],0,5) . "</td>
					<td>" . substr($fila["hasta"],0,5) . "</td>
					<td>" . $fila["id_horario"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='horario-editar.php?id=" . $fila["id_horario"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarHorario(" . '"' . $fila["id_horario"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($horario);