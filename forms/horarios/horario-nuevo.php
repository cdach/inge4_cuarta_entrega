<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Horario</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
                    <label for="">Taller</label>
					<div class="col-xs-1">
                        <?php 
							@session_start();
							fn_lista_combo("id_taller","select id_taller,nombre from talleres order by 2",$_SESSION["taller"],""); 
						?>
                        <small id="id_taller_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
                    <label for="">Dia</label>
                    <div class="col-xs-1">
                        <select id="dia" name="dia" class="form-control">
                            <option value="1" selected>Domingo</option>
                            <option value="2">Lunes</option>
                            <option value="3">Martes</option>
                            <option value="4">Miércoles</option>
                            <option value="5">Jueves</option>
                            <option value="6">Viernes</option>
                            <option value="7">Sábado</option>
                        </select>
                        <small id="dia_ayuda" class="form-text text-muted"></small>
                    </div>
                </div>
				<div class="form-group">
					<label>Desde</label>
					<input type="time" class="form-control" id="desde"/>
				</div>
				<div class="form-group">
					<label>Hasta</label>
					<input type="time" class="form-control" id="hasta"/>
				</div>				
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoHorario();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='horario-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#id_taller").select2();
		$("#dia").select2();
		$("#id_taller").focus();
	</script>
</html>