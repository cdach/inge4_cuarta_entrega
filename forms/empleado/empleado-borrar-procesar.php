<?php
	include_once("../../clases/Empleado.php");
	
	$id = $_POST["id"];
	$empleado = new Empleado();
	
	
	if($empleado->borrarEmpleado($id)){
		auditoriaRegistro('EMPLEADOS','B','Empleado Borrado: ' . $id);
		unset($empleado);
		echo "<script> listarEmpleados(); </script>";
	}
?>