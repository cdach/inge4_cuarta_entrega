<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Empleado</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre y Apellido del Empleado" maxlength="50">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Documento</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="documento" name="documento"
						placeholder="Ej.: 4123789" maxlength="10">
						<small id="documento_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Dirección</label>
					<div class="col-xs-1">
						<textarea class="form-control" id="direccion" name="direccion"
						placeholder="Dirección" rows="2"></textarea>
						<small id="direccion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Localidad</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="localidad" name="localidad"
						placeholder="Ej.: Mi Localidad" maxlength="50">
						<small id="localidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Telefono Principal</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_principal" name="telefono_principal"
						placeholder="Ej.: 021595636" maxlength="15">
						<small id="telefono_principal_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Telefono Secundario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_secundario" name="telefono_secundario"
						placeholder="Ej.: 0981662323" maxlength="15">
						<small id="telefono_secundario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<div class="col-xs-1">
						<input type="email" class="form-control" id="email" name="email"
						placeholder="Ej.: micorreo@dominio.com" maxlength="50">
						<small id="email_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cargo</label>
					<div class="col-xs-1">
						<select id="cargo" class="form-control">
							<option value="M" selected>Mecánico</option>
							<option value="C">Chapista</option>
							<option value="P">Pintor</option>
							<option value="A">Administrador</option>
						</select>
						<small id="cargo_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" class="form-control">
							<option value="I">Inactivo</option>
							<option value="A" selected>Activo</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>			
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoEmpleado();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='empleado-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#cargo").select2();
		$("#estado").select2();
		$("#nombre").focus();
	</script>
</html>