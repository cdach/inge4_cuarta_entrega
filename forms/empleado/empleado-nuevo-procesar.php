<?php
	include_once("../../lib/funciones.php");
	
	$nombre 			= $_POST["nombre"];
	$documento 			= $_POST["documento"];
	$direccion 			= $_POST["direccion"];
	$localidad 			= $_POST["localidad"];
	$telefono_principal = $_POST["telefono_principal"];
	$telefono_secundario = $_POST["telefono_secundario"];
	$email 				= $_POST["email"];
	$cargo 				= $_POST["cargo"];
	$estado 			= $_POST["estado"];
	
	include_once("empleado-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Empleado.php");
	$empleado = new Empleado("",$nombre,$documento,$direccion,$localidad,
	$telefono_principal,$telefono_secundario,$email,$cargo,$estado);
		
	if($empleado->grabarEmpleado()){
		auditoriaRegistro('EMPLEADOS','G','Empleado agregado: ' . $nombre);
		unset($empleado);
		echo "<script> location.href='empleado-lista.php';</script>";
	}
?>