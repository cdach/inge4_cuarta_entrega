<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
			
			include_once("../../clases/Empleado.php");
			$id = $_GET["id"];
			$empleado= new Empleado();
			$empleado->recuperarEmpleado($id);
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Editar Empleado</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Empleado</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="id_empleado" name="id_empleado" 
						placeholder="ID de Empleado" maxlength="20" value="<?php echo $empleado->getIdEmpleado(); ?>"
						readonly>
						<small id="id_empleado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre y Apellido del Empleado" maxlength="50" value="<?php echo $empleado->getNombre(); ?>">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Documento</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="documento" name="documento"
						placeholder="Documento" maxlength="10" value="<?php echo $empleado->getDocumento(); ?>">
						<small id="documento" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Dirección</label>
					<div class="col-xs-1">
						<textarea class="form-control" id="direccion" name="direccion"
						placeholder="Dirección" rows="2"><?php echo $empleado->getDireccion(); ?></textarea>
						<small id="direccion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Localidad</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="localidad" name="localidad"
						placeholder="Localidad" maxlength="50" value="<?php echo $empleado->getLocalidad(); ?>">
						<small id="localidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Teléfono Principal</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_principal" name="telefono_principal"
						placeholder="Telefono Principal" maxlength="15" value="<?php echo $empleado->getTelefonoPrincipal(); ?>">
						<small id="telefono_principal_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Teléfono Secundario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_secundario" name="telefono_secundario"
						placeholder="Telefono Secundario" maxlength="15" value="<?php echo $empleado->getTelefonoSecundario(); ?>">
						<small id="telefono_secundario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<div class="col-xs-1">
						<input type="email" class="form-control" id="email" name="email"
						placeholder="Ej.: micorreo@dominio.com" maxlength="50" value="<?php echo $empleado->getEmail(); ?>">
						<small id="email_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cargo</label>
					<div class="col-xs-1">
						<select id="cargo" name="cargo" class="form-control">
							<option value="M"<?php echo 'M' === $empleado->getCargo() ? "selected='selected'" : ""; ?> >Mecánico</option>
							<option value="C"<?php echo 'C' === $empleado->getCargo() ? "selected='selected'" : ""; ?> >Chapista</option>
							<option value="P"<?php echo 'P' === $empleado->getCargo() ? "selected='selected'" : ""; ?> >Pintor</option>
							<option value="A"<?php echo 'A' === $empleado->getCargo() ? "selected='selected'" : ""; ?> >Administrador</option>
						</select>
						<small id="cargo_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" name="estado" class="form-control">
							<option value="A"<?php echo 'A' === $empleado->getEstado() ? "selected='selected'" : ""; ?> >Activo</option>
							<option value="I"<?php echo 'I' === $empleado->getEstado() ? "selected='selected'" : ""; ?> >Inactivo</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>			
				<br>
				<button type="button" class="btn btn-primary" onclick="editarEmpleado();">Editar</button>
				<button type="button" class="btn btn-success" onclick="location.href='empleado-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#cargo").select2();
		$("#estado").select2();
		$("#nombre").focus();
	</script>
</html>