<?php
	include_once("../../lib/funciones.php");
	
	$id_empleado		= $_POST["id_empleado"];
	$nombre 			= $_POST["nombre"];
	$documento 			= $_POST["documento"];
	$direccion 			= $_POST["direccion"];
	$localidad			= $_POST["localidad"];
	$telefono_principal = $_POST["telefono_principal"];
	$telefono_secundario = $_POST["telefono_secundario"];
	$email		 		= $_POST["email"];
	$cargo		 		= $_POST["cargo"];
	$estado 			= $_POST["estado"];
	
	include_once("empleado-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Empleado.php");
	$empleado = new Empleado($id_empleado,$nombre,$documento,$direccion,$localidad,
	$telefono_principal,$telefono_secundario,$email,$cargo,$estado);
		
	if($empleado->EditarEmpleado()){
		auditoriaRegistro('EMPLEADOS','M','Empleado editado: ' . $id_empleado);
		unset($empleado);
		echo "<script> location.href='empleado-lista.php';</script>";
	}
?>