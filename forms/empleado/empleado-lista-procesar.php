<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Empleado.php");
	
	$empleado = new Empleado();
	$rs = $empleado->listarEmpleados();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Documento</th>
                <th>Dirección</th>
				<th>Cargo</th>
                <th>Estado</th>
                <th>ID</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
			if($fila["cargo"] === "A")
				$cargo = "Administrador";
			elseif($fila["cargo"] === "M")
				$cargo = "Mecánico";
			elseif($fila["cargo"] === "P")
				$cargo = "Pintor";
			elseif($fila["cargo"] === "C")
				$cargo = "Chapista";
			
			$estado = (($fila["estado"] === "A") ? "Activo" : "Inactivo");
				
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["documento"] . "</td>
					<td>" . $fila["direccion"] . "</td>
					<td>" . $cargo . "</td>
					<td>" . $estado . "</td>
					<td>" . $fila["id_empleado"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='empleado-editar.php?id=" . $fila["id_empleado"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarEmpleado(" . '"' . $fila["id_empleado"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($empleado);