<?php
	require_once("../../lib/nusoap/lib/nusoap.php");

	function llamarConsultarHorario($id_taller,$dia){
		
		$wsdl = "http://localhost/taller/forms/webservice/servicio.php?wsdl";
		
		$client = new nusoap_client($wsdl,"wsdl");
		$params = array("id_taller"=>$id_taller, "dia"=>$dia);
		$response = $client->call("consultarHorario",$params);
		
		return $response;
	}	
?>