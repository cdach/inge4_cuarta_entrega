<?php
	require_once("../../lib/nusoap/lib/nusoap.php");
	$server = new soap_server();
	
	function consultarHorario($id_taller,$dia)
	{
		
		include_once("../../clases/Conexion.php");
		$conn = new Conexion();
		
		$sql = "select	t.nombre,
						(case when h.dia = '1' then 'Domingo' 
						 when h.dia = '2' then 'Lunes'
						 when h.dia = '3' then 'Martes'
						 when h.dia = '4' then 'Miércoles'
						 when h.dia = '5' then 'Jueves'
						 when h.dia = '6' then 'Viernes'
						 when h.dia = '7' then 'Sábado'
						end) as nombre_dia,
						h.desde,
						h.hasta
				from	horarios h, talleres t
				where	h.id_taller = t.id_taller ";
		
		if($id_taller > 0)
			$sql .= " and h.id_taller = '" . $id_taller . "'";
		
		if($dia != "")
			$sql .= " and h.dia = '" . $dia . "'";
				
		$sql .= " order by 	t.nombre, h.dia ";
		
		$row = $conn->consultarSql($sql);
				
		$rs = "";
		if($row)
			foreach($row as $fila){
				$rs .=  $fila["nombre"] . "#" . $fila["nombre_dia"] . "#" . $fila["desde"] . "#" . $fila["hasta"] . "|";
			}
		else
			$rs = "NO SE ENCONTRARON DATOS PARA LA CONSULTA";
		
		$rs = substr($rs, 0, -1);
		return $rs;
	}
	
	$ns = "http://localhost/taller/forms/webservice/servicio.php";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace="$ns";
	
	$server->register("consultarHorario",array('id_taller'=>'xsd:int', 
	'dia'=>'xsd:String'),
	array('return'=>'xsd:String'),$ns);
	
	if(isset($HTTP_RAW_POST_DATA)){
		$input = $HTTP_RAW_POST_DATA;
	}
	else{
		$input = implode("rn",file('php://input'));
	}
	$server->service($input);
	exit;
?>