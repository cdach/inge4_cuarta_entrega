<?php
	require_once("cliente.php");
		
	$id_taller 	= $_POST["id_taller"];
	$dia 		= $_POST["dia"];
	
	$datos = llamarConsultarHorario($id_taller,$dia);
	// Separa por filas los datos
	$datos = explode("|",$datos);
	
	$rs="<table class='table table-dark'>
		<thead>
			<tr>
				<th scope='col'>Taller</th>
				<th scope='col'>Día</th>
				<th scope='col'>Desde</th>
				<th scope='col'>Hasta</th>
			</tr>
		</thead>
		<tbody>";
		
	foreach($datos as $fila){
		// Separa por columnas
		$columna = explode("#",$fila);
		
		$rs .= "<tr>
					<td>" . $columna[0] . "</td>
					<td>" . $columna[1] . "</td>
					<td>" . $columna[2] . "</td>
					<td>" . $columna[3] . "</td>
				</tr>";
	}
	
	$rs .= "</tbody>
			</table>";
	
	echo $rs;
?>