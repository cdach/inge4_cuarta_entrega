<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
		
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="../../js/datetimepicker/jquery.datetimepicker.css">
		<script src="../../js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Servicio WEB - Consulta de Horarios</h1>
			<form class="horizontal-form" method="post" action="rpt-auditorias-procesar.php">
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_taller","select id_taller, nombre from talleres order by 2",
						0,""); ?>	
					</div>
				</div>
				<div class="form-group">
					<label for="">Día</label>
					<div class="col-xs-1">
						<select class="form-control" id="dia" name="dia">
							<option value=""></option>
							<option value="1">Domingo</option>
							<option value="2">Lunes</option>
							<option value="3">Martes</option>
							<option value="4">Miércoles</option>
							<option value="5">Jueves</option>
							<option value="6">Viernes</option>
							<option value="7">Sábado</option>
						</select>
					</div>
				</div>
				<br>
				<button type="button" class="btn btn-secondary" onclick="llamarServicio();" value="">+ VER</button>
				<br>
				<br>
				<div id="rs-ajax"></div>
			</form>
		</div>
	</body>
	<script>
		$("#id_taller").focus();
	</script>
</html>