<?php
	include_once("../../lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../lib/phpjasperxml/setting.php");
	include_once("../../lib/funciones.php");

	$id_usuario	= $_POST["id_usuario"];
	$desde		= $_POST["desde"];
	$hasta 		= $_POST["hasta"];
	$tabla 		= $_POST["tabla"];
			
	$sql = "select a.id_usuario,
			to_char(a.fecha,'DD/MM/YYYY HH:MI') as fecha,
			a.tabla,
			(case
			when a.accion = 'G' then 'INSERCIÓN' 
			when a.accion = 'M' then 'MODIFICACIÓN'
			when a.accion = 'B' then 'BORRADO'
			when a.accion = 'E' then 'ENTRADA'
			when a.accion = 'S' then 'SALIDA'
			end) as accion,
			a.descricpcion
			from auditorias a 
			where 1 = 1 ";

	if($id_usuario != "") $sql .= " and id_usuario = '" . $id_usuario . "'";
	if($desde != "" && $desde != "__/__/____ __:__") $sql .= " and fecha >= '" . $desde . "'";
	if($hasta != "" && $hasta != "__/__/____ __:__") $sql .= " and fecha <= '" . $hasta . "'";
	if($tabla != "") $sql .= " and tabla = '" . $tabla . "'";
			
	$sql .= " order by fecha, accion";
		
	include_once("../../clases/Conexion.php");
	$conn = new Conexion();
			
	$row = $conn->consultarSql($sql);
	if(!$row){
		echo "<script> alert('Reporte. No se encontraron Datos para el Rango!!!. Verifique'); 
		location.href='rpt-auditorias.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-auditorias.jrxml");
	$PHPJasperXML->sql=$sql;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($conn);
?>