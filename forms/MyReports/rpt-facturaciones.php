<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Reporte de Facturaciones</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form" method="post" action="rpt-facturaciones-procesar.php">
			<div class="form-group">
				<div class="form-group">
					<label for="">Venta</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="id_venta" 
						name="id_venta" size="5" maxlength="5"> 	
					</div>
				</div>
				<div class="form-group">
					<label for="">Número de Factura</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("numero_factura","select distinct numero_factura,numero_factura from ventas order by 2",0,
						"",""); ?>	
					</div>
				</div>
					<div class="form-group">
					<label for="">Desde</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="desde" 
						name="desde" placeholder="dd/mm/aaaa hh:mm" value=""> 
					</div>
				</div>
				<div class="form-group">
					<label for="">Hasta</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="hasta" 
						name="hasta" placeholder="dd/mm/aaaa hh:mm" value=""> 
					</div>
				</div>
				<label for="">Cliente</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_cliente","select id_cliente,nombre from clientes order by 2",
						0,""); ?>	
					</div>
				<div class="form-group">
					<label for="">Presupuesto</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_presupuesto","select id_presupuesto,id_presupuesto from presupuestos order by 2",
						0,""); ?> 	
					</div>
				</div>
				<div class="form-group">
					<label for="">Marca</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_marca","select id_marca,nombre from vehiculos_marcas order by 2",
						0,"filtrarModelo();"); ?>	
					</div>
				</div>
				<div class="form-group">
					<label for="">Modelo</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_modelo","select id_modelo,nombre from vehiculos_modelos order by 2",
						0,""); ?>	
					</div>
				</div>
				<br>
				<button type="submit" class="btn btn-secondary" value="">+ VER</button>
			</form>
		</div>
	</body>
	<script>
		$("#numero_factura").select2();
		$("#id_presupuesto").select2();
		$("#id_cliente").select2();
		$("#id_marca").select2();
		$("#id_modelo").select2();
		$("select#numero_factura")[0].selectedIndex = 0;
		$("select#id_presupuesto")[0].selectedIndex = 0;
		$("select#id_cliente")[0].selectedIndex = 0;
		$("select#id_marca")[0].selectedIndex = 0;
		$("select#id_modelo")[0].selectedIndex = 0;
		
		$("#id_venta").focus();
	</script>
</html>