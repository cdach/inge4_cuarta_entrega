<?php
	include_once("../../lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../lib/phpjasperxml/setting.php");
	include_once("../../lib/funciones.php");

	$id_usuario	= $_POST["id_usuario"];
	$perfil 	= $_POST["perfil"];
			
	$sql = "select	u.id_usuario,
					u.nombre,
					to_char(u.fecha_registro,'DD/MM/YYYY HH:MM') as fecha_registro,
					(case
						when u.estado = 'A' then 'Activo'
						when u.estado = 'I' then 'Inactivo'
						else 'Desconocido'
					end) as estado,
					(case
						when u.perfil = 'A' then 'Administrador'
						when u.perfil = 'O' then 'Operativo'
						when u.perfil = 'C' then 'Consulta'
						else 'Desconocido'
					end) as perfil
			from 	usuarios as u
			where 	1 = 1 ";

	if($id_usuario != "") $sql .= " and u.id_usuario = '" . $id_usuario . "'";
	if($perfil != "") $sql .= " and u.perfil = '" . $perfil . "'";
			
	$sql .= " order by 1";
	
		
	include_once("../../clases/Conexion.php");
	$conn = new Conexion();
			
	$row = $conn->consultarSql($sql);
	if(!$row){
		echo "<script> alert('Reporte. No se encontraron Datos para el Rango!!!. Verifique'); 
		location.href='rpt-usuarios.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-usuarios.jrxml");
	$PHPJasperXML->sql=$sql;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($conn);
?>