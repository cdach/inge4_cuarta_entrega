<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
				
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
		
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="../../js/datetimepicker/jquery.datetimepicker.css">
		<script src="../../js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Ventas Diarias</h1>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Desde</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="desde" 
						name="desde" placeholder="dd/mm/aaaa hh:mm" maxlength="16" value=""> 
					</div>
				</div>
				<div class="form-group">
					<label for="">Hasta</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="hasta" 
						name="hasta" placeholder="dd/mm/aaaa hh:mm" maxlength="16" value=""> 
					</div>
				</div>
				<label for="">Servicio</label>
				<div class="col-xs-1">
					<?php fn_lista_combo("id_servicio_producto","select id_servicio_producto, nombre from servicios_productos order by 2",
					0,""); ?>	
				</div>
				<br>
				<button type="button" class="btn btn-secondary" onclick="verGrafico1();" value="">+ VER</button>
				<div id="rs-ajax"></div>
			</form>
		</div>
	</body>
	<script>
		jQuery("#desde").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		jQuery("#hasta").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		$("#id_servicio_producto").select2();
		$("#id_servicio_producto").focus();
	</script>
</html>