<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Reporte de Usuarios</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form" method="post" action="rpt-usuarios-procesar.php">
				<div class="form-group">
					<label for="">Usuario</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_usuario","select id_usuario,id_usuario from usuarios order by 2",
						0,""); ?>	
					</div>
				</div>
				<div class="form-group">
					<label for="">Perfil</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("perfil", "select distinct perfil, (case when perfil = 'A' then 'Administrador' 
											when perfil = 'O' then 'Operativo' when perfil = 'C' then 'Consulta' end) as nombre
											from usuarios order by 1",0,""); ?>	
					</div>
				</div>
				<br>
				<button type="submit" class="btn btn-secondary" value="">+ VER</button>
			</form>
		</div>
	</body>
	<script>
		$("select#id_usuario")[0].selectedIndex = 0;
		$("select#perfil")[0].selectedIndex = 0;
		$("#id_usuario").select2();
		$("#perfil").select2();
		$("#id_usuario").focus();
	</script>
</html>