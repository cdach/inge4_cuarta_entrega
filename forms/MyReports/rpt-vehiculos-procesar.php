<?php
	include_once("../../lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../lib/phpjasperxml/setting.php");
	include_once("../../lib/funciones.php");

	$id_cliente	= $_POST["id_cliente"];
	$id_marca 	= $_POST["id_marca"];
	$id_modelo 	= $_POST["id_modelo"];
	$id_color 	= $_POST["id_color"];
			
	$sql = "select 	cli.nombre,
					(case 
						when cli.digito <> '' then cli.documento || '-' || cli.digito
						else cli.documento
					 end) as documento,
					 cli.direccion,
					 cli.telefono_principal,
					 cli.email,
					 veh.chapa,
					 mar.nombre as nombre_marca,
					 mod.nombre as nombre_modelo,
					 col.nombre as nombre_color,
					 veh.anio		
			from 	clientes as cli, vehiculos veh, vehiculos_marcas mar, vehiculos_modelos as mod, 
					vehiculos_colores as col
			where	cli.id_cliente = veh.id_cliente
			and		veh.id_marca = mar.id_marca
			and		veh.id_modelo = mod.id_modelo
			and		veh.id_color = col.id_color ";

	if($id_cliente != "") $sql .= " and cli.id_cliente = '" . $id_cliente . "'";
	if($id_marca != "") $sql .= " and veh.id_marca = '" . $id_marca . "'";
	if($id_modelo != "") $sql .= " and veh.id_modelo = '" . $id_modelo . "'";
	if($id_color != "") $sql .= " and veh.id_color = '" . $id_color . "'";
				
	$sql .= " order by 1,6";
	
		
	include_once("../../clases/Conexion.php");
	$conn = new Conexion();
			
	$row = $conn->consultarSql($sql);
	if(!$row){
		echo "<script> alert('Reporte. No se encontraron Datos para el Rango!!!. Verifique'); 
		location.href='rpt-vehiculos.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-vehiculos.jrxml");
	$PHPJasperXML->sql=$sql;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($conn);
?>