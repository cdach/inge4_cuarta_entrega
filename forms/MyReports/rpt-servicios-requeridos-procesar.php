<?php
	include_once("../../clases/Conexion.php");
	include_once("../../lib/funciones.php");
		
	$desde 					= $_POST["desde"];
	$hasta 					= $_POST["hasta"];
	$id_servicio_producto 	= $_POST["id_servicio_producto"];
	$estado				 	= $_POST["estado"];
	
	$sql = "select s.nombre,
			sum(cantidad) as cantidad
			from   presupuestos p, presupuestos_detalles pd, servicios_productos s
			where  p.id_presupuesto = pd.id_presupuesto
			and	   pd.id_servicio_producto = s.id_servicio_producto ";
			
	if($desde != "" && $desde != "__/__/____ __:__") $sql .= " and p.fecha >= '" . $desde . "' ";
	if($hasta != "" && $hasta != "__/__/____ __:__") $sql .= " and p.fecha <= '" . $hasta . "' ";
	if($id_servicio_producto != "") $sql .= " and pd.id_servicio_producto = '" . $id_servicio_producto . "'";
	if($estado != "") $sql .= " and p.estado <= '" . $estado . "' ";
	$sql .= " group by nombre order by cantidad desc";

	$conn = new Conexion();
	$row = $conn->consultarSql($sql);
	if(!$row){
		fn_mensaje("Aviso","No se encontraron Datos.");
		return;
	}
	
	$data = "";
	$apendice = "";
	$x = -100;
	foreach($row as $datos){
		if($datos["cantidad"] > $x){
			$x = $datos["cantidad"];
			$apendice = "sliced: true, selected: true ";
		}
		else
		{
			$apendice = "";
		}
		
		$data .= "{
					name: '" . $datos['nombre'] . "',
					y: " . $datos['cantidad'] . "," . $apendice . "
				},";
	}
	
	$rs = "<!DOCTYPE HTML>
		<html>
			<head>
				<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
				<meta name='viewport' content='width=device-width, initial-scale=1'>
				<meta charset='utf-8'>
				<title>Highcharts Example</title>

				<style type='text/css'>
				.highcharts-figure, .highcharts-data-table table {
					min-width: 320px; 
					max-width: 800px;
					margin: 1em auto;
				}

				.highcharts-data-table table {
					font-family: Verdana, sans-serif;
					border-collapse: collapse;
					border: 1px solid #EBEBEB;
					margin: 10px auto;
					text-align: center;
					width: 100%;
					max-width: 500px;
				}
				
				.highcharts-data-table caption {
					padding: 1em 0;
					font-size: 1.2em;
					color: #555;
				}
				
				.highcharts-data-table th {
					font-weight: 600;
					padding: 0.5em;
				}
				
				.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
					padding: 0.5em;
				}
				
				.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
					background: #f8f8f8;
				}
				
				.highcharts-data-table tr:hover {
					background: #f1f7ff;
				}

				input[type='number'] {
					min-width: 50px;
				}
						</style>
					</head>
					<body>
				<script src='../../js/Highcharts/code/highcharts.js'></script>
				<script src='../../js/Highcharts/code/modules/exporting.js'></script>
				<script src='../../js/Highcharts/code/modules/export-data.js'></script>
				<script src='../../js/Highcharts/scode/modules/accessibility.js'></script>

				<figure class='highcharts-figure'>
					<div id='container'></div>
					<p class='highcharts-description'></p>
				</figure>

						<script type='text/javascript'>
				Highcharts.chart('container', {
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false,
						type: 'pie'
					},
					title: {
						text: 'Servicios Requeridos'
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					accessibility: {
						point: {
							valueSuffix: '%'
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								format: '<b>{point.name}</b>: {point.percentage:.1f} %'
							}
						}
					},
					series: [{
						name: 'Brands',
						colorByPoint: true,
						data: [" . $data . "]
					}]
				});
				</script>
			</body>
		</html>";
		
	echo $rs;
?>