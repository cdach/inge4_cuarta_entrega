<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
		
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="../../js/datetimepicker/jquery.datetimepicker.css">
		<script src="../../js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Reporte de Auditorías</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form" method="post" action="rpt-auditorias-procesar.php">
				<div class="form-group">
					<label for="">Usuario</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_usuario","select id_usuario,id_usuario from usuarios order by 2",
						0,""); ?>	
					</div>
				</div>
				<div class="form-group">
					<label for="">Desde</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="desde" 
						name="desde" placeholder="dd/mm/aaaa hh:mm" maxlength="16" value=""> 
					</div>
				</div>
				<div class="form-group">
					<label for="">Hasta</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="hasta" 
						name="hasta" placeholder="dd/mm/aaaa hh:mm" maxlength="16" value=""> 
					</div>
				</div>
				<div class="form-group">
					<label for="">Tabla</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("tabla","select distinct tabla,tabla from auditorias order by 2",
						0,""); ?>	
					</div>
				</div>
				<br>
				<button type="submit" class="btn btn-secondary" value="">+ VER</button>
			</form>
		</div>
	</body>
	<script>
		jQuery("#desde").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		jQuery("#hasta").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		
		$("select#id_usuario")[0].selectedIndex = 0;
		$("select#tabla")[0].selectedIndex = 0;
		$("#id_usuario").select2();
		$("#tabla").select2();
		$("#id_usuario").focus();
	</script>
</html>