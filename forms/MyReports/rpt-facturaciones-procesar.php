<?php
	include_once("../../lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../lib/phpjasperxml/setting.php");
	include_once("../../lib/funciones.php");

	$id_venta			= $_POST["id_venta"];
	$numero_factura		= $_POST["numero_factura"];
	$desde				= $_POST["desde"];
	$hasta 				= $_POST["hasta"];
	$id_presupuesto		= $_POST["id_presupuesto"];
	$id_cliente			= $_POST["id_cliente"];
	$id_marca			= $_POST["id_marca"];
	$id_modelo			= $_POST["id_modelo"];
			
	$sql = "select	ve.id_venta,
					ve.id_presupuesto,
					ta.nombre as nombre_taller,
					to_char(ve.fecha,'dd/mm/yyyy hh:mm') as fecha,
					ve.numero_factura,
					cl.nombre as nombre_cliente,
					(case 
						when digito <> '' then cl.documento || '-' || digito 
						else cl.documento
					 end) as documento,
					cl.direccion,
					cl.telefono_principal,
					cl.email,
					vh.chapa,
					vm.nombre as nombre_marca,
					vo.nombre as nombre_modelo,
					vc.nombre as nombre_color,
					em.nombre as nombre_empleado,
					(case
						when ve.condicion = 'C' then 'Contado'
						when ve.condicion = 'R' then 'Credito'
					end) as condicion,
					ve.observacion,
					(case
						when ve.estado = 'P' then 'Pendiente'
						when ve.estado = 'C' then 'Cancelado'
					end) as estado,
					to_char(ve.total, '999,999,999') as total,
					sp.nombre as nombre_servicio,
					vd.cantidad,
					to_char(vd.precio,'999,999,999') as precio,
					to_char(vd.subtotal,'999,999,999')as subtotal,
					to_char((select sum(d.subtotal) from ventas_detalles as d),'999G999G999') as total_general			
			from	ventas as ve,
					presupuestos as pe,
					ventas_detalles as vd, 
					talleres as ta,
					clientes as cl, 
					empleados as em,
					servicios_productos as sp,
					vehiculos as vh,
					vehiculos_marcas as vm,
					vehiculos_modelos as vo,
					vehiculos_colores as vc
			where	ve.id_venta = vd.id_venta
			and		ve.id_taller = ta.id_taller
			and 	ve.id_presupuesto = pe.id_presupuesto
			and		pe.id_vehiculo = vh.id_vehiculo
			and		vh.id_marca = vm.id_marca
			and		vh.id_modelo = vo.id_modelo
			and		vh.id_color = vc.id_color
			and		ve.id_cliente = cl.id_cliente
			and		ve.facturado_por = em.id_empleado
			and		vd.id_servicio_producto = sp.id_servicio_producto";
	
	if($id_venta != "") $sql .= " and ve.id_venta = '" . $id_venta . "'";
	if($numero_factura != "") $sql .= " and ve.numero_factura = '" . $numero_factura . "'";
	if($desde != "" && $desde != "__/__/____ __:__") $sql .= " and ve.fecha >= '" . $desde . "'";
	if($hasta != "" && $hasta != "__/__/____ __:__") $sql .= " and ve.fecha <= '" . $hasta . "'";
	if($id_presupuesto != "") $sql .= " and ve.id_presupuesto = '" . $id_presupuesto . "'";
	if($id_cliente != "") $sql .= " and ve.id_cliente = '" . $id_cliente . "'";
	if($id_marca != "") $sql .= " and vh.id_marca = '" . $id_marca . "'";
	if($id_modelo != "") $sql .= " and vh.id_modelo = '" . $id_modelo . "'";
			
	$sql .= " order by 2,4,1";
	
	include_once("../../clases/Conexion.php");
	$conn = new Conexion();
			
	$row = $conn->consultarSql($sql);
	if(!$row){
		echo "<script> alert('Reporte. No se encontraron Datos para el Rango!!!. Verifique'); 
		location.href='rpt-facturaciones.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-facturaciones.jrxml");
	$PHPJasperXML->sql=$sql;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($conn);
?>