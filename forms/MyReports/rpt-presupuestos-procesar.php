<?php
	include_once("../../lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../lib/phpjasperxml/setting.php");
	include_once("../../lib/funciones.php");

	$id_presupuesto	= $_POST["id_presupuesto"];
	$desde			= $_POST["desde"];
	$hasta 			= $_POST["hasta"];
	$id_cliente		= $_POST["id_cliente"];
	$id_marca		= $_POST["id_marca"];
	$id_modelo		= $_POST["id_modelo"];
			
	$sql = "select 	pe.id_presupuesto, 
			ta.nombre as nombre_taller,
			cl.nombre as nombre_cliente,
			(case when digito <> '' then cl.documento || '-' || digito 
			 else cl.documento
			 end) as documento,
			cl.direccion,
			cl.telefono_principal,
			cl.email,
			ve.chapa,
			mr.nombre as nombre_marca,
			mo.nombre as nombre_modelo,
			co.nombre as nombre_color,
			em.nombre as nombre_empleado,
			to_char(pe.fecha,'DD/MM/YYYY HH:MI')as fecha,
			to_char(pe.total,'999G999G999')as total,
			(case
				when pe.condicion = 'C' then 'Contado'
				when pe.condicion = 'R' then 'Crédito'
			end) as condicion,
			(case
				when pe.estado = 'P' then 'Pendiente'
				when pe.estado = 'A' then 'Aprobado'
				when pe.estado = 'R' then 'Rechazado'
			end) as estado,
			pe.observacion,
			sp.nombre as nombre_servicio,
			pd.cantidad,
			to_char(pd.precio,'999G999G999') as precio,
			to_char(pd.subtotal,'999G999G999') as subtotal,
			to_char((select sum(d.subtotal) from presupuestos_detalles as d
			where d.id_presupuesto = pe.id_presupuesto),'999G999G999') as totales,
			to_char((select sum(d.subtotal) from presupuestos_detalles as d),'999G999G999') as total_general			
	from 	presupuestos as pe, 
			presupuestos_detalles as pd, 
			talleres as ta, 
			clientes as cl, 
			vehiculos as ve, 
			vehiculos_marcas as mr,
			vehiculos_modelos as mo,
			empleados as em,
			servicios_productos as sp,
			vehiculos_colores as co
	where	pe.id_taller = ta.id_taller
	and		pe.id_cliente = cl.id_cliente
	and		pe.id_vehiculo = ve.id_vehiculo
	and		pe.presupuestado_por = em.id_empleado
	and		pe.id_presupuesto = pd.id_presupuesto
	and		pd.id_servicio_producto = sp.id_servicio_producto
	and		ve.id_marca = mr.id_marca
	and		ve.id_modelo = mo.id_modelo
	and 	ve.id_color = co.id_color";
	
	if($id_presupuesto != "") $sql .= " and pe.id_presupuesto = '" . $id_presupuesto . "'";
	if($desde != "" && $desde != "__/__/____ __:__") $sql .= " and pe.fecha >= '" . $desde . "'";
	if($hasta != "" && $hasta != "__/__/____ __:__") $sql .= " and pe.fecha <= '" . $hasta . "'";
	if($id_cliente != "") $sql .= " and pe.id_cliente = '" . $id_cliente . "'";
	if($id_marca != "") $sql .= " and ve.id_marca = '" . $id_marca . "'";
	if($id_modelo != "") $sql .= " and ve.id_modelo = '" . $id_modelo . "'";
			
	$sql .= " order by nombre_taller,id_presupuesto";
		
	include_once("../../clases/Conexion.php");
	$conn = new Conexion();
			
	$row = $conn->consultarSql($sql);
	if(!$row){
		echo "<script> alert('Reporte. No se encontraron Datos para el Rango!!!. Verifique'); 
		location.href='rpt-presupuestos.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-presupuestos.jrxml");
	$PHPJasperXML->sql=$sql;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($conn);
?>