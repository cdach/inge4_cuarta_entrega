<?php
	include_once("../../clases/Conexion.php");
	include_once("../../lib/funciones.php");
		
	$desde 					= $_POST["desde"];
	$hasta 					= $_POST["hasta"];
	$id_servicio_producto 	= $_POST["id_servicio_producto"];
	
	$sql = "select to_char(v.fecha,'dd/mm/yyyy') as fecha,
			sum(vd.subtotal) as total
			from ventas as v, ventas_detalles vd
			where v.id_venta = vd.id_venta ";
			
	if($desde != "" && $desde != "__/__/____ __:__") $sql .= " and v.fecha >= '" . $desde . "' ";
	if($hasta != "" && $hasta != "__/__/____ __:__") $sql .= " and v.fecha <= '" . $hasta . "' ";
	if($id_servicio_producto != "") $sql .= " and vd.id_servicio_producto = '" . $id_servicio_producto . "'";
	$sql .= " group by fecha order by fecha";

	$conn = new Conexion();
	$row = $conn->consultarSql($sql);
	if(!$row)
	{
		fn_mensaje("Aviso","No se encontraron Datos.");
		return;
	}
	
	$categorias = "[";
	$data = "[";
	
	foreach($row as $datos){
		$categorias .= "'" . $datos["fecha"] . "',";
		$data .= $datos["total"] . ",";
	}
	
	$categorias .= "]";
	$data .= "]";
	
	$rs = "<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<title></title>

		<style type='text/css'>
			.highcharts-figure, .highcharts-data-table table {
				min-width: 310px; 
				max-width: 800px;
				margin: 1em auto;
			}

			#container {
				height: 400px;
			}

			.highcharts-data-table table {
				font-family: Verdana, sans-serif;
				border-collapse: collapse;
				border: 1px solid #EBEBEB;
				margin: 10px auto;
				text-align: center;
				width: 100%;
				max-width: 500px;
			}
			.highcharts-data-table caption {
				padding: 1em 0;
				font-size: 1.2em;
				color: #555;
			}
			.highcharts-data-table th {
				font-weight: 600;
				padding: 0.5em;
			}
			.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
				padding: 0.5em;
			}
			.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
				background: #f8f8f8;
			}
			.highcharts-data-table tr:hover {
				background: #f1f7ff;
			}
		</style>
	</head>
	<body>
		<script src='../../js/Highcharts/code/highcharts.js'></script>
		<script src='../../js/Highcharts/code/modules/exporting.js'></script>
		<script src='../../js/Highcharts/code/modules/export-data.js'></script>
		<script src='../../js/Highcharts/code/modules/accessibility.js'></script>

		<figure class='highcharts-figure'>
			<div id='container'></div>
			<p class='highcharts-description'></p>
		</figure>

		<script type='text/javascript'>
			Highcharts.chart('container', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'Ventas Diarias'
			},
			xAxis: {
				categories: " . $categorias . " ,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Ingresos'
				}
			},
			tooltip: {
				headerFormat: '<span style=font-size:10px>{point.key}</span><table>',
				pointFormat: '<tr><td style=color:{series.color};padding:0>{series.name}: </td>' +
					'<td style=padding:0><b>{point.y:.0f}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Monto',
				data: " . $data . "

			}]
		});
				</script>
			</body>
		</html>";
		
	echo $rs;
	
	
	
	/*$rs = "<script src='../../js/highcharts/highcharts.js'></script>
	<script src='../../code/modules/exporting.js'></script>
	<div id='container' style='min-width: 300px; height: 400px; margin: 0 auto'></div>
	<script type='text/javascript'>
		Highcharts.chart('container', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'Proyectos por monto presupuestado'
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -45,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Monto (millones)'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Monto: <b>{point.y:.1f} Millones</b>'
			},
			series: [{
				name: 'Population',
				data:[";
				
			$filas = pg_num_rows($result);
			if($filas > 0)
			{
				for($i=0;$i<$filas;$i++)
				{
					$registro = pg_fetch_array($result,$i);
					$rs .= "['" . $registro["nombre"] . "', " . $registro["monto_presupuesto"] . "]";
					if($i != $filas - 1)
						$rs .= ",";
				}
			}
			$rs .= 	"],
				dataLabels: {
					enabled: true,
					rotation: -90,
					color: '#FFFFFF',
					align: 'right',
					format: '{point.y:.1f}',
					y: 10,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			}]
		});
	</script>";
	
	echo $rs;*/
?>