<?php
	include_once("../../lib/funciones.php");
	
	$nombre 		= $_POST["nombre"];
	
	include_once("marca-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Marca.php");
	$marca = new Marca("",$nombre);
		
	if($marca->grabarMarca()){
		unset($marca);
		echo "<script> location.href='marca-lista.php';</script>";
	}
?>