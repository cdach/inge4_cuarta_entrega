<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/VehiculoModelo.php");
	
	$vehiculomodelo = new VehiculoModelo();
	$rs = $vehiculomodelo->listarVehiculosModelos();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Modelo</th>
                <th>Marca</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
        foreach($rs as $fila){


            echo "<tr>
                    <td>" . $fila["nombre"] . "</td>
                    <td>" . $fila["nombre_marca"] . "</td>
                    <td>
                        <a class='btn btn-warning btn-sm' href='vehiculomodelo-editar.php?id=" . $fila["id_modelo"] . "'
                        data-toggle='tooltip' title='Editar'>
                        <i class='fa fa-edit'></i></a>
                        <a class='btn btn-danger btn-sm' onclick='borrarVehiculoModelo(" . '"' . $fila["id_modelo"] . '"' . ");'
                        data-toggle='tooltip' title='Borrar'>
                        <i class='fa fa-trash'></i></a>
                    </td>
                 </tr>";
        }
    }
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($vehiculomodelo);