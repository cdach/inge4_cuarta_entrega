<?php
	include_once("../../lib/funciones.php");
	
	$id_marca		= $_POST["id_marca"];
	$nombre 		= $_POST["nombre"];
	
	include_once("vehiculomodelo-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/VehiculoModelo.php");

	$vehiculomodelo = new VehiculoModelo("",$id_marca,$nombre);
		
	if($vehiculomodelo->grabarVehiculoModelo()){
		unset($vehiculomodelo);
		echo "<script> location.href='vehiculomodelo-lista.php';</script>";
	}
?>