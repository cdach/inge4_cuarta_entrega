<?php
	include_once("../../lib/funciones.php");
	
	$id_usuario 	= $_POST["id_usuario"];
	$nombre 		= $_POST["nombre"];
	$clave 			= $_POST["clave"];
	$confirmacion 	= $_POST["confirmacion"];
	$fecha_registro = $_POST["fecha_registro"];
	$estado 		= $_POST["estado"];
	$perfil 		= $_POST["perfil"];
	
	include_once("usuario-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Usuario.php");
	// Se asume que si la longitud es 20 o menos ctres es una modificación
	if(strlen($clave) <= 20) $clave = sha1($clave);
	if(strlen($confirmacion) <= 20) $confirmacion = sha1($confirmacion);
		
	$usuario = new Usuario($id_usuario,$nombre,$clave,$confirmacion,
	$fecha_registro,$estado,$perfil);
		
	if($usuario->editarUsuario()){
		auditoriaRegistro('USUARIOS','M','Usuario modificado: ' . $id_usuario);
		unset($usuario);
		echo "<script> location.href='usuario-lista.php';</script>";
	}
?>