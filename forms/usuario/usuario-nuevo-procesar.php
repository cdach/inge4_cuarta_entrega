<?php
	include_once("../../lib/funciones.php");
	
	$id_usuario 	= $_POST["id_usuario"];
	$nombre 		= $_POST["nombre"];
	$clave 			= $_POST["clave"];
	$confirmacion 	= $_POST["confirmacion"];
	$fecha_registro = $_POST["fecha_registro"];
	$estado 		= $_POST["estado"];
	$perfil 		= $_POST["perfil"];
	
	include_once("usuario-validaciones.php");
	if(!$controlError) return;
	
	$fecha_registro = fn_formato_fecha($fecha_registro,true);
		
	include_once("../../clases/Usuario.php");
	$clave = sha1($clave);
	$confirmacion = sha1($confirmacion);
		
	$usuario = new Usuario($id_usuario,$nombre,$clave,$confirmacion,
	$fecha_registro,$estado,$perfil);
	
	if($usuario->grabarUsuario()){
		auditoriaRegistro('USUARIOS','G','Usuario agregado: ' . $id_usuario);
		unset($usuario);
		echo "<script> location.href='usuario-lista.php';</script>";
	}	
?>