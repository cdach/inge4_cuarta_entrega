<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Usuario.php");
	
	$id = $_POST["id"];
	$usuario = new Usuario();
	
	if($id == "root"){
		echo "<script> alert('No puede eliminar el usuario root.'); </script>";
		return;
	}
	
	if($usuario->borrarUsuario($id)){
		auditoriaRegistro('USUARIOS','B','Usuario Borrado: ' . $id);
		unset($usuario);
		echo "<script> listarUsuarios(); </script>";
	}
?>