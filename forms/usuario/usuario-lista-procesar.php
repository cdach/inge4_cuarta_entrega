<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Usuario.php");
	
	$usuario = new Usuario();
	$rs = $usuario->listarUsuarios();
	
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Registro</th>
                <th>Estado</th>
                <th>Perfil</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
			$estado = (($fila["estado"] === "A") ? "Activo" : "Inactivo");
			$perfil = $fila["perfil"];
			if($perfil == "A") $perfil = "Administrador";
			elseif($perfil == "O") $perfil = "Operativo";
			elseif($perfil == "C") $perfil = "Consulta";
			
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["id_usuario"] . "</td>
					<td>" . fn_formato_fecha($fila["fecha_registro"]) . "</td>
					<td>" . $estado . "</td>
					<td>" . $perfil . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='usuario-editar.php?id=" . $fila["id_usuario"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarUsuario(" . '"' . $fila["id_usuario"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($usuario);