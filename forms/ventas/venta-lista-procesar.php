<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Venta.php");
	$venta = new Venta();
	$rs = $venta->listarVentas();
	
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Numero de facura</th>
                <th>Cliente</th>
                <th>Fecha</th>
                <th>Total</th>
                <th>Condición</th>
                <th>Estado</th>
                <th>ID</th>
                <th>Accion</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila){
			
			
			
			echo "<tr>
					<td>" . $fila["numero_factura"] . "</td>
					<td>" . $fila["nombre_cliente"] . "</td>
					<td>" . $fila["fecha"] . "</td>
					<td>" . $fila["total"] . "</td>
					<td>" . $fila["condicion"] . "</td>
					<td>" . $fila["estado"] . "</td>
					<td>" . $fila["id_venta"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='venta-editar.php?id=" . $fila["id_venta"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarVenta(" . '"' . $fila["id_venta"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>	
					</td>
				 </tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($venta);