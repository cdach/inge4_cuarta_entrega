<?php
	$controlError = true;
	
	echo "<script>
			$('#id_taller_ayuda').html('');
			$('#numero_factura_ayuda').html('');
			$('#id_presupuesto_ayuda').html('');
			$('#id_cliente_ayuda').html('');
			$('#fecha_ayuda').html('');
			$('#facturado_por_ayuda').html('');
			$('#total_ayuda').html('');
			$('#condicion_ayuda').html('');
			$('#observacion_ayuda').html('');
			$('#estado_ayuda').html('');
		 </script>";
			
	if($id_taller == ""){
		echo "<script> $('#id_taller_ayuda').html('Debe ingresar el taller!!!'); 
		$('#id_taller').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($numero_factura == ""){
		echo "<script> $('#numero_factura_ayuda').html('Debe ingresar el numero de la factura!!!'); 
		$('#numero_factura').focus(); </script>";
		$controlError = false;
		return;
	}
	
	
	if($id_cliente == ""){
		echo "<script> $('#id_cliente_ayuda').html('Debe ingresar el cliente!!!'); 
		$('#id_cliente').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($fecha == ""){
		echo "<script> $('#fecha_ayuda').html('Debe ingresar la fecha!!!'); 
		$('#fecha').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($facturado_por == ""){
		echo "<script> $('#facturado_por_ayuda').html('Debe ingresar por quien fue facturado!!!'); 
		$('#fecha').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if(!fn_validar_dato($total,"integer") || $total < 0){
		echo "<script> $('#total_ayuda').html('Debe ingresar el total!!!'); 
		$('#total').focus(); </script>";
		$controlError = false;
		return;
	}
	
		if($condicion == ""){
		echo "<script> $('#condicion_ayuda').html('Debe ingresar la condicion!!!'); 
		$('#condicion').focus(); </script>";
		$controlError = false;
		return;
	}
	
		if($estado == ""){
		echo "<script> $('#estado_ayuda').html('Debe ingresar el estado!!!'); 
		$('#estado').focus(); </script>";
		$controlError = false;
		return;
	}
?>