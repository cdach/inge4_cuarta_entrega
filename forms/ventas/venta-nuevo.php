<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nueva Factura</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_taller","select id_taller,nombre from talleres order by 2",
						0,""); ?>
						<small id="id_taller_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Numero de Factura</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="numero_factura" name="numero_factura"
						placeholder="001-001-0000001" maxlength="15">
						<small id="numero_factura_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Presupuesto</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_presupuesto","select id_presupuesto,from presupuestos order by 2",
						0,""); ?>
						<small id="id_presupuesto_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cliente</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_cliente","select id_cliente,nombre from clientes order by 2",
						0,""); ?>
						<small id="id_cliente_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Fecha</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="fecha" 
						name="fecha" value="<?php echo date("d/m/Y H:m");?>" 
						placeholder="fecha" disabled>
						<small id="fecha_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Facturado Por</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("facturado_por","select id_empleado,nombre from empleados order by 2",
						0,""); ?>
						<small id="facturado_por_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Total</label>
					<div class="col-xs-1">
						<input type="number" class="form-control" id="total" name="total"
						placeholder="Entre 0 y 999.999.999" maxlength="12" value="0">
						<small id="total_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Condicion</label>
					<div class="col-xs-1">
						<select id="condicion" name="condicion" class="form-control">
							<option value="C" selected>Contado</option>
							<option value="R">Credito</option>
							
						</select>
						<small id="condicion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Observacion</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="observacion" name="observacion"
						placeholder="observacion" maxlength="150">
						<small id="observacion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="condicion" name="condicion" class="form-control">
							<option value="A" selected>Aprobado</option>
							<option value="P">Pendiente</option>
							<option value="R">Rechazado</option>
							
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevaVenta();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='venta-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		jQuery("#fecha").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		$("#id_taller").select2();
		$("#id_presupuesto").select2();
		$("#id_cliente").select2();
		$("#facturado_por").select2();
		$("#condicion").select2();
		$("#estado").select2();
		$("#id_taller").focus();
	</script>
</html>