<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
			
			include_once("../../clases/Venta.php");
			$id = $_GET["id"];
			$venta = new Venta();
			$venta->recuperarVentas($id);
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Editar Venta</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Venta</label>
					<div class="col-xs-1">
						<input type="int" class="form-control" id="id_venta" name="id_venta" 
						placeholder="ID de Venta" maxlength="20" value="<?php echo $venta->getIdVenta(); ?>"
						readonly>
						<small id="id_venta_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_taller","select id_taller,nombre from talleres order by 2",
						$venta->getIdTaller(),""); ?>
						<small id="id_taller_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Numero de Factura</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="numero_factura" name="numero_factura"
						placeholder="Numero_factura" maxlength="16" value="<?php echo $venta->getNumeroFactura(); ?>">
						<small id="numero_factura_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Presupuesto</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_presupuesto","select id_presupuesto,nombre from presupuestos order by 2",
						$venta->getIdPresupuesto(),""); ?>
						<small id="id_presupuesto_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				
				<div class="form-group">
					<label for="">Cliente</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_cliente","select id_cliente,nombre from clientes order by 2",
						$venta->getIdCliente(),""); ?>
						<small id="id_cliente_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">fecha</label>
					<div class="col-xs-1">
						<input type="time" class="form-control" id="fecha" name="fecha"
						placeholder="fecha" rows="5" value="<?php echo substr($venta->getFecha(),0,5); ?>"/>
						<small id="fecha_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Facturado Por</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="facturado_por" name="facturado_por"
						placeholder="facturado_por" maxlength="" value="<?php echo $venta->getFacturadoPor(); ?>">
						<small id="numero_factura_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Total</label>
					<div class="col-xs-1">
						<input type="number" class="form-control" id="total" name="total"
						placeholder="Entre 0 y 999.999.999" maxlength="12" value="<?php echo $venta->getTotal(); ?>">
						<small id="total_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Condicion</label>
					<div class="col-xs-1">
						<select id="condicion" name="condicion" class="form-control">
							<option value="C"<?php echo 'C' === $venta->getCondicion() ? "selected='selected'" : ""; ?> >Contado</option>
							<option value="R"<?php echo 'R' === $venta->getCondicion() ? "selected='selected'" : ""; ?> >Credito</option>
						</select>
						<small id="condicion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Observacion</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="observacion" name="observacion"
						placeholder="observacion" maxlength="" value="<?php echo $venta->getObservacion(); ?>">
						<small id="observacion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" name="estado" class="form-control">
							<option value="A"<?php echo 'A' === $venta->getCondicion() ? "selected='selected'" : ""; ?> >Aprobado</option>
							<option value="P"<?php echo 'P' === $venta->getCondicion() ? "selected='selected'" : ""; ?> >Pendiente</option>
							<option value="R"<?php echo 'R' === $venta->getCondicion() ? "selected='selected'" : ""; ?> >Rechazado</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<br>
				<button type="button" class="btn btn-primary" onclick="editarVenta();">Editar</button>
				<button type="button" class="btn btn-success" onclick="location.href='venta-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		jQuery("#fecha").datetimepicker({format:"d/m/Y H:i",mask:true,theme:'dark'});
		$("#id_taller").select2();
		$("#id_presupuesto").select2();
		$("#id_cliente").select2();
		$("#facturado_por").select2();
		$("#condicion").select2();
		$("#estado").select2();
		$("#id_taller").focus();
	</script>