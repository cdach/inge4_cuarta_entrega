<?php
	include_once("../../lib/funciones.php");
	
	$id_taller			= $_POST["id_taller"];
	$numero_factura 	= $_POST["numero_factura"];
	$id_presupuesto 	= $_POST["id_presupuesto"];
	$id_cliente			= $_POST["id_cliente"];
	$fecha 				= $_POST["fecha"];
	$facturado_por 		= $_POST["facturado_por"];
	$total 				= $_POST["total"];
	$condicion 			= $_POST["condicion"];
	$observacion 		= $_POST["observacion"];
	$estado 			= $_POST["estado"];
	
	include_once("venta-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Venta.php");
		
	$venta = new Venta("",$id_taller,$numero_factura,$id_presupuesto,
									$id_cliente,$fecha,$facturado_por,$total,$condicion,$observacion,$estado);
		
	if($venta->grabarVenta()){
		auditoriaRegistro('VENTAS','G','Venta agregada: ' . $facturado_por);
		unset($venta);
		echo "<script> location.href='venta-lista.php';</script>";
	}
?>