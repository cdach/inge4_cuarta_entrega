<?php
	include_once("../../clases/ProductoServicio.php");
	
	$id = $_POST["id"];
	$producto = new ProductoServicio();
	
	
	if($producto->borrarProductoServicio($id)){
		auditoriaRegistro('SERVICIOS_PRODUCTOS','B','Producto/Servicio Borrado: ' . $id);
		unset($producto);
		echo "<script> listarProductoServicio(); </script>";
	}
?>