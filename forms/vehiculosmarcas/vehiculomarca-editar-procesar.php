<?php
	include_once("../../lib/funciones.php");
	
	$id_marca 	= $_POST["id_marca"];
	$nombre 		= $_POST["nombre"];
	
	include_once("vehiculomarca-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/VehiculoMarca.php");	
	$vehiculomarca = new VehiculoMarca($id_marca,$nombre);
		
	if($vehiculomarca->editarVehiculoMarca()){
		unset($vehiculomarca);
		echo "<script> location.href='vehiculomarca-lista.php';</script>";
	}
?>