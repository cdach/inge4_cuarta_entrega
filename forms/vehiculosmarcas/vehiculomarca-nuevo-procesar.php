<?php
	include_once("../../lib/funciones.php");
	
	$nombre 		= $_POST["nombre"];
	
	include_once("vehiculomarca-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/VehiculoMarca.php");
	$vehiculomarca = new VehiculoMarca("",$nombre);
		
	if($vehiculomarca->grabarVehiculoMarca()){
		unset($vehiculomarca);
		echo "<script> location.href='vehiculomarca-lista.php';</script>";
	}
?>