<?php
	include_once("../../lib/funciones.php");
	
	$nombre 	= $_POST["nombre"];
	$codigo		= $_POST["codigo"];
	
	include_once("color-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Color.php");

	$color = new Color("",$nombre,$codigo);
		
	if($color->grabarColor()){
		unset($color);
		echo "<script> location.href='color-lista.php';</script>";
	}
?>