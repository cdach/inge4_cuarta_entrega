<?php
	include_once("../../lib/funciones.php");
	
	$id_color 	= $_POST["id_color"];
	$nombre 	= $_POST["nombre"];
	$codigo 	= $_POST["codigo"];
	
	include_once("color-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Color.php");	
	$color = new Color($id_color,$nombre,$codigo);
		
	if($color->editarColor()){
		unset($color);
		echo "<script> location.href='color-lista.php';</script>";
	}
?>