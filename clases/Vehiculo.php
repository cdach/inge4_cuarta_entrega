<?php
	include_once("Conexion.php");
	
	class Vehiculo{
		private $id_vehiculo;
		private $id_marca;
		private $id_modelo;
		private $id_color;
		private $chapa;
		private $anio;
		private $chasis;
		private $id_cliente;
		private $conexion;

		// Constructor
		function __construct($id_vehiculo="",$id_marca="",$id_modelo="",$id_color="",$chapa="",
		$anio="",$chasis="",$id_cliente=""){
			$this->id_vehiculo = $id_vehiculo;
			$this->id_marca = $id_marca;
			$this->id_modelo = $id_modelo;
			$this->id_color = $id_color;
			$this->anio = $anio;
			$this->chapa = $chapa;
			$this->chasis = $chasis;
			$this->id_cliente = $id_cliente;			
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdVehiculo(){
			return $this->id_vehiculo;
		}
		
		
		function getIdMarca(){
			return $this->id_marca;
		}
		
		function getIdModelo(){
			return $this->id_modelo;
		}
		
		function getIdColor(){
			return $this->id_color;
		}
		
		function getChapa(){
			return $this->chapa;
		}
		
		function getAnio(){
			return $this->anio;
		}
		
		function getChasis(){
			return $this->chasis;
		}
		
		function getIdCliente(){
			return $this->id_cliente;
		}
		
		// Métodos Setters
		function setIdVehiculo($id_vehiculo){
			$this->id_vehiculo = $id_vehiculo;
		}
		
		function setIdMarca($id_marca){
			$this->id_marca = $id_marca;
		}
		
		function setIdModelo($id_modelo){
			$this->id_modelo = $id_modelo;
		}
		
		function setIdColor(){
			$this->id_color;
		}
		
		function setChapa($chapa){
			$this->chapa = $chapa;
		}

		function setAnio($anio){
			$this->anio = $anio;
		}
		
		function setChasis($chasis){
			$this->chasis = $chasis;
		}
		
		function setIdCliente($id_cliente){
			$this->id_cliente = $id_cliente;
		}
		
		// Trae el vehiculo
		function recuperarVehiculo($id){
			$sql = "select * from vehiculos where id_vehiculo = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_vehiculo = $fila[0]["id_vehiculo"];
				$this->id_marca = $fila[0]["id_marca"];
				$this->id_modelo = $fila[0]["id_modelo"];
				$this->id_color = $fila[0]["id_color"];
				$this->chapa = $fila[0]["chapa"];
				$this->anio = $fila[0]["anio"];
				$this->chasis = $fila[0]["chasis"];
				$this->id_cliente = $fila[0]["id_cliente"];
			}
		}
		
		// Lista todos los vehiculos
		function listarVehiculos(){
			$sql = "select 	ve.id_vehiculo, 
							ve.chapa,
							vma.nombre as nombre_marca, 
							vmo.nombre as nombre_modelo, 
							vmc.nombre as nombre_color, 
							ve.chapa, 
							ve.anio, 
							ve.chasis,
							cli.nombre as nombre_cliente
					from 	vehiculos as ve left outer join clientes cli on ve.id_cliente = cli.id_cliente, 
							vehiculos_marcas as vma, vehiculos_modelos as vmo, 
							vehiculos_colores as vmc
					where	ve.id_marca = vma.id_marca
					and 	ve.id_modelo = vmo.id_modelo
					and		ve.id_color = vmc.id_color 
					order by ve.chapa";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarVehiculo(){
			$sql = "insert into vehiculos 
			(id_marca,id_modelo,id_color,chapa,anio,chasis,id_cliente)
			values(	'$this->id_marca',
					'$this->id_modelo',
					'$this->id_color',
					'$this->chapa',
					'$this->anio',
					'$this->chasis',
					'$this->id_cliente')";
					
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarVehiculo(){
			$sql = "update vehiculos
					set id_marca = '$this->id_marca',
					id_modelo = '$this->id_modelo',
					id_color = '$this->id_color',
					chapa = '$this->chapa',
					anio = '$this->anio',
					chasis = '$this->chasis',
					id_cliente = '$this->id_cliente'
				where id_vehiculo = '$this->id_vehiculo'";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		function borrarVehiculo($id){
			$sql = "delete from vehiculos where id_vehiculo ='" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>