<?php
	include_once("Conexion.php");
	
	class Presupuesto{
		private $id_presupuesto;
		private $id_taller;
		private $id_cliente;
		private $id_vehiculo;
		private $fecha;
		private $fecha_validacion;
		private $presupuestado_por;
		private $total;
		private $condicion;
		private $observacion;
		private $estado;
		private $conexion;

		// Consructor
		function __construct($id_presupuesto="",$id_taller="",$id_cliente="",$id_vehiculo="",
		$fecha="",$fecha_validacion="",$presupuestado_por="",$total="",$condicion="",$observacion="",$estado=""){
			$this->id_presupuesto = $id_presupuesto;
			$this->id_taller = $id_taller;
			$this->id_cliente = $id_cliente;
			$this->id_vehiculo = $id_vehiculo;
			$this->fecha = $fecha;
			$this->fecha_validacion = $fecha_validacion;
			$this->presupuestado_por = $presupuestado_por;
			$this->total = $total;
			$this->condicion = $condicion;
			$this->observacion = $observacion;
			$this->estado = $estado;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdPresupuesto(){
			return $this->id_presupuesto;
		}
		
		function getIdTaller(){
			return $this->id_taller;
		}
		
		function getIdCliente(){
			return $this->id_cliente;
		}
		
		function getIdVehiculo(){
			return $this->id_vehiculo;
		}

		function getFecha(){
			return $this->fecha;
		}
		
		function getFechaValidez(){
			return $this->fecha_validez;
		}
				
		function getPresupuestadoPor(){
			return $this->presupuestado_por;
		}
				
		function getTotal(){
			return $this->total;
		}
		
		function getCondicion(){
			return $this->condicion;
		}
		
		function getObservacion(){
			return $this->observacion;
		}

		function getEstado(){
			return $this->estado;
		}
		
		// Métodos Setters
		function setIdPresupuesto($id_presupuesto){
			$this->id_presupuesto = $id_presupuesto;
		}
		
		function setIdCliente($id_cliente){
			$this->id_cliente = $id_cliente;
		}
		
		function setIdTaller($id_taller){
			$this->id_taller = $id_taller;
		}
		
		
		function setIdVehiculo($id_vehiculo){
			$this->id_vehiculo = $id_vehiculo;
		}
		
		function setFecha($fecha){
			$this->fecha = $fecha;
		}
				
		function setIdFechaValidez($fecha_validez){
			$this->fecha_validez = $fecha_validez;
		}
		
		function setPresupuestadoPor(){
			return $this->presupuestado_por = $presupuestado_por;
		}		
		
		function setTotal($total){
			$this->total = $total;
		}
				
		function setCondicion($condicion){
			$this->condicion = $condicion;
		}
		
		function setObservacion($observacion){
			$this->observacion = $observacion;
		}
						
		function setEstado($estado){
			$this->estado = $estado;
		}
			
		// Trae el servicio_producto
		function recuperarPresupuesto($id){
			$sql = "select * from presupuestos where id_presupuesto = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_presupuesto = $fila[0]["id_presupuesto"];
				$this->id_taller = $fila[0]["id_taller"];
				$this->id_cliente = $fila[0]["id_cliente"];
				$this->id_vehiculo = $fila[0]["id_vehiculo"];
				$this->fecha = $fila[0]["fecha"];
				$this->fecha_validacion = $fila[0]["fecha_validacion"];
				$this->presupuestado_por = $fila[0]["presupuestado_por"];
				$this->total = $fila[0]["total"];
				$this->condicion = $fila[0]["condicion"];
				$this->observacion = $fila[0]["observacion"];
				$this->estado = $fila[0]["estado"];
			}
		}
		
		// Lista todos los servicios_productos
		function listarPresupuestos(){
			$sql = "select 	pe.id_presupuesto as id, 
		cl.nombre as nombre_cliente,
		ve.chapa as chapa,
		mr.nombre as nombre_marca,
		mo.nombre as nombre_modelo,
		to_char(pe.fecha,'DD/MM/YYYY HH:MM')as fecha,
		to_char(pe.total,'999G999G999')as total,
		(case
			when pe.condicion = 'C' then 'Contado'
			when pe.condicion = 'R' then 'Crédito'
		end) as condicion,
		(case
			when pe.estado = 'P' then 'Pendiente'
			when pe.estado = 'A' then 'Aprobado'
		 	when pe.estado = 'R' then 'Rechazado'
		end) as estado
		from 	presupuestos as pe, 
		clientes as cl, 
		vehiculos as ve, 
		vehiculos_marcas as mr,
		vehiculos_modelos as mo
		where	pe.id_cliente = cl.id_cliente
		and		pe.id_vehiculo = ve.id_vehiculo
		and		ve.id_marca = mr.id_marca
		and		ve.id_modelo = mo.id_modelo";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarPresupuesto(){
			$sql = "insert into presupuestos(id_presupuesto,id_taller,id_cliente,id_vehiculo,
			fecha,fecha_validacion,presupuestado_por,total,condicion,observacion,estado)
			values('$this->id_presupuesto',
			'$this->id_taller',
			'$this->id_cliente',
			'$this->id_vehiculo',
			'$this->fecha',
			'$this->fecha_validez',
			'$this->presupuestado_por',
			'$this->total',
			'$this->condicion',
			'$this->observacion',
			'$this->estado')";
			//echo $sql; return;	
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarPresupuestos(){
			$sql = "update presupuestos
			set id_taller = '$this->id_taller',
			id_cliente = '$this->id_cliente',
			id_vehiculo = '$this->id_vehiculo',
			fecha = '$this->fecha',
			fecha_validez = '$this->fecha_validez',
			presupuestado_por = '$this->presupuestado_por',
			total = '$this->total',
			condicion = '$this->condicion',
			observacion = '$this->observacion',
			estado = '$this->estado,
			where id_presupuesto = '$this->id_presupuesto'";
			// echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarPresupuesto($id){
			$sql = "delete from presupuestos where id_presupuesto = '$id'";
			// echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
		
	}
?>