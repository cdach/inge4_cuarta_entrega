<?php
	include_once("Conexion.php");
	
	class unidad{
		private $id_unidad;
		private $nombre;
		private $sigla;
		private $conexion;

		// Constructor
		function __construct($id_unidad="",$nombre="",$sigla=""){
			$this->id_unidad = $id_unidad;
			$this->nombre = $nombre;
			$this->sigla = $sigla;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdUnidad(){
			return $this->id_unidad;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getSigla(){
			return $this->sigla;
		}
		
		// Métodos Setters
		function setIdUnidad($id_unidad){
			$this->id_unidad = $id_unidad;
		}
		
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function setSigla($sigla){
			$this->sigla = $sigla;
		}
		
		// Trae la unidad
		function recuperarUnidad($id){
			$sql = "select * from unidades where id_unidad = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_unidad = $fila[0]["id_unidad"];
				$this->nombre = $fila[0]["nombre"];
				$this->sigla = $fila[0]["sigla"];
			}
		}
		
		// Lista todas las unidades
		function listarUnidad(){
			$sql = "select id_unidad, nombre, sigla
			from unidades order by nombre";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarUnidad(){
			$sql = "insert into unidades(nombre,sigla)
			values('$this->nombre',
			'$this->sigla')";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarUnidad(){
			$sql = "update unidades
			set nombre = '$this->nombre',
			sigla = '$this->sigla'
			where id_unidad = '$this->id_unidad'";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		function borrarUnidad($id){
			$sql = "delete from unidades where id_unidad ='" . $id . "'";
			echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>