<?php
	include_once("Conexion.php");
	
	class Marca{
		private $id_marca;
		private $nombre;
		
		
		function __construct($id_marca="",$nombre=""){
			$this->id_marca = $id_marca;
			$this->nombre = $nombre;
			$this->conexion = new Conexion();
		}
			
		function __destruct(){
			$this->conexion = null;
		}
			
		// Métodos Getters
		function getIdMarca(){
			return $this->id_marca;
		}
			
		function getNombre(){
			return $this->nombre;
		}
		
		function setIdMarca($id_marca){
			$this->id_marca = $id_marca;
		}
			
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function recuperarMarca($id){
			$sql = "select * from marcas where id_marca = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_marca = $fila[0]["id_marca"];
				$this->nombre = $fila[0]["nombre"];
			}
		}
		
		function listarMarca(){
			$sql = "select id_marca, nombre from marcas order by nombre";
				
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}

		function grabarMarca(){
			$sql = "insert into marcas(nombre)
			values('$this->nombre')";
			
			return $this->conexion->consultarSql($sql,false);
		}
			
		// Editar el registro
		function editarMarca(){
			$sql = "update marcas
			set nombre = '$this->nombre'
			where id_marca = '$this->id_marca'";
				
			return $this->conexion->consultarSql($sql,false);
		}
			
			// Borrar el registro
		function borrarMarca($id){
			$sql = "delete from marcas where id_marca = '" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>