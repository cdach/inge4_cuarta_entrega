<?php
	include_once("Conexion.php");
	
	class Usuario{
		private $id_usuario;
		private $nombre;
		private $clave;
		private $confirmacion;
		private $fecha_registro;
		private $estado;
		private $perfil;
		private $conexion;

		// Constructor
		function __construct($id_usuario="",$nombre="",$clave="",$confirmacion="",
		$fecha_registro="",$estado="",$perfil=""){
			$this->id_usuario = $id_usuario;
			$this->nombre = $nombre;
			$this->clave = $clave;
			$this->confirmacion = $confirmacion;
			$this->fecha_registro = $fecha_registro;
			$this->estado = $estado;
			$this->perfil = $perfil;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdUsuario(){
			return $this->id_usuario;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getClave(){
			return $this->clave;
		}
		
		function getConfirmacion(){
			return $this->confirmacion;
		}
		
		function getFechaRegistro(){
			return $this->fecha_registro;
		}
		
		function getEstado(){
			return $this->estado;
		}
		
		function getPerfil(){
			return $this->perfil;
		}
		
		// Métodos Setters
		function setIdUsuario($id_usuario){
			$this->id_usuario = $id_usuario;
		}
		
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function setClave($clave){
			$this->clave = $clave;
		}
		
		function setConfirmacion($confirmacion){
			$this->confirmacion = $confirmacion;
		}
		
		function setFechaRegistro($fecha_registro){
			$this->fecha_registro = $fecha_registro;
		}
		
		function setEstado($estado){
			$this->estado = $estado;
		}
		
		function setPerfil($perfil){
			$this->perfil = $perfil;
		}
		
		// Trae el usuario
		function recuperarUsuario($id){
			$sql = "select * from usuarios where id_usuario = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_usuario = $fila[0]["id_usuario"];
				$this->nombre = $fila[0]["nombre"];
				$this->clave = $fila[0]["clave"];
				$this->confirmacion = $fila[0]["confirmacion"];
				$this->fecha_registro = $fila[0]["fecha_registro"];
				$this->estado = $fila[0]["estado"];
				$this->perfil = $fila[0]["perfil"];
			}
		}
		
		// Lista todos los usuarios
		function listarUsuarios(){
			$sql = "select id_usuario, nombre, fecha_registro, estado, perfil
			from usuarios order by nombre";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarUsuario(){
			$sql = "insert into usuarios
			values('$this->id_usuario',
			'$this->nombre',
			'$this->clave',
			'$this->confirmacion',
			'$this->fecha_registro',
			'$this->estado',
			'$this->perfil')";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarUsuario(){
			$sql = "update usuarios
			set nombre = '$this->nombre',
			clave = '$this->clave',
			confirmacion = '$this->confirmacion',
			fecha_registro = '$this->fecha_registro',
			estado = '$this->estado',
			perfil = '$this->perfil'
			where id_usuario = '$this->id_usuario'";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		function borrarUsuario($id){
			$sql = "delete from usuarios where id_usuario ='" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>