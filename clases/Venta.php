<?php
	include_once("Conexion.php");
	
	class Venta{
		private $id_venta;
		private $id_taller;
		private $numero_factura;
		private $id_presupuesto;
		private $id_cliente;
		private $fecha;
		private $facturado_por;
		private $total;
		private $condicion;
		private $observacion;
		private $estado;
		private $conexion;

		// Consructor
		function __construct($id_venta="",$id_taller="",$numero_factura="",$id_presupuesto="",
		$id_cliente="",$fecha="",$facturado_por="",$total="",$condicion="",$observacion="",$estado=""){
			$this->id_venta = $id_venta;
			$this->id_taller = $id_taller;
			$this->numero_factura = $numero_factura;
			$this->id_presupuesto = $id_presupuesto;
			$this->id_cliente = $id_cliente;
			$this->fecha = $fecha;
			$this->facturado_por = $facturado_por;
			$this->total = $total;
			$this->condicion = $condicion;
			$this->observacion = $observacion;
			$this->estado = $estado;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdVenta(){
			return $this->id_venta;
		}
		
		function getIdTaller(){
			return $this->id_taller;
		}
		
		function getNumeroFactura(){
			return $this->numero_factura;
		}
		
		function getIdPresupuesto(){
			return $this->id_presupuesto;
		}

		function getIdCliente(){
			return $this->id_cliente;
		}
		
		function getFecha(){
			return $this->fecha;
		}
				
		function getFacturadoPor(){
			return $this->facturado_por;
		}
				
		function getTotal(){
			return $this->total;
		}
		
		function getCondicion(){
			return $this->condicion;
		}
		
		function getObservacion(){
			return $this->observacion;
		}
		function getEstado(){
			return $this->estado;
		}
		
		// Métodos Setters
		function setIdVenta($id_venta){
			$this->id_venta = $id_venta;
		}
		
		function setIdTaller($id_taller){
			$this->id_taller = $id_taller;
		}
		
		function setNumeroFactura($numero_factura){
			$this->numero_factura = $numero_factura;
		}
		
		function setIdPresupuesto($id_presupuesto){
			$this->id_presupuesto = $id_presupuesto;
		}
		
		function setIdCliente($id_cliente){
			$this->id_cliente = $id_cliente;
		}
				
		function setFecha($fecha){
			$this->fecha = $fecha;
		}
				
		function setFacturadoPor($facturado_por){
			$this->facturado_por = $facturado_por;
		}
		
		function setTotal($total){
			$this->total = $total;
		}
				
		function setCondicion($condicion){
			$this->condicion = $condicion;
		}
						
		function setObservacion($observacion){
			$this->observacion = $observacion;
		}
		
		function setEstado($estado){
			$this->estado = $estado;
		}
		
		// Trae el servicio_producto
		function recuperarVentas($id){
			$sql = "select * from ventas where id_venta = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_venta = $fila[0]["id_venta"];
				$this->id_taller = $fila[0]["id_taller"];
				$this->numero_factura = $fila[0]["numero_factura"];
				$this->id_presupuesto = $fila[0]["id_presupuesto"];
				$this->id_cliente = $fila[0]["id_cliente"];
				$this->fecha = $fila[0]["fecha"];
				$this->facturado_por = $fila[0]["facturado_por"];
				$this->total = $fila[0]["total"];
				$this->condicion = $fila[0]["condicion"];
				$this->observacion = $fila[0]["observacion"];
				$this->estado = $fila[0]["estado"];
			}
		}
		
		// Lista todos los ventas
		function listarVentas(){
			$sql = "select	ve.id_venta as id_venta,
					to_char(ve.fecha,'dd/mm/yyyy hh:mm') as fecha,
					ve.numero_factura as numero_factura,
					cl.nombre as nombre_cliente,
					(case
						when ve.condicion = 'C' then 'Contado'
						when ve.condicion = 'R' then 'Credito'
					end) as condicion,
					(case
						when ve.estado = 'A' then 'Aprobado'
						when ve.estado = 'P' then 'Pendiente'
						when ve.estado = 'R' then 'Rechazado'
					end) as estado,
					to_char(ve.total, '999,999,999') as total
			from	ventas as ve,
					presupuestos as pe,
					clientes as cl
			where	ve.id_presupuesto = pe.id_presupuesto
			and		ve.id_cliente = cl.id_cliente";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarVenta(){
			$sql = "insert into ventas(id_taller,numero_factura,id_presupuesto,id_cliente,fecha,
			facturado_por,total,condicion,observacion,estado)
			values('$this->id_taller',
			'$this->numero_factura',
			'$this->id_presupuesto',
			'$this->id_cliente',
			'$this->fecha',
			'$this->facturado_por',
			'$this->total',
			'$this->condicion',
			'$this->observacion',
			'$this->estado')";
			// echo $sql; return;	
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarVenta(){
			$sql = "update ventas
			set id_taller = '$this->id_taller',
			numero_factura = '$this->numero_factura',
			id_presupuesto = '$this->id_presupuesto',
			id_cliente = '$this->id_cliente',
			fecha = '$this->fecha',
			facturado_por = '$this->facturado_por',
			total = '$this->total',
			condicion = '$this->condicion',
			observacion = '$this->observacion',
			estado = '$this->estado'
			where id_venta = '$this->id_venta'";
			// echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarProductoServicio($id){
			$sql = "delete from ventas where id_venta = '$id'";
			// echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
		
	}
?>