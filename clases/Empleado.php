<?php
	include_once("Conexion.php");
	
	class Empleado{
		private $id_empleado;
		private $nombre;
		private $documento;
		private $direccion;
		private $localidad;
		private $telefono_principal;
		private $telefono_secundario;
		private $email;
		private $cargo;
		private $estado;
		private $conexion;

		// Constructor
		function __construct($id_empleado="",$nombre="",$documento="",$direccion="",$localidad="",
			$telefono_principal="",$telefono_secundario="",$email="",$cargo="",$estado=""){
			$this->id_empleado = $id_empleado;
			$this->nombre = $nombre;
			$this->documento = $documento;
			$this->direccion = $direccion;
			$this->localidad = $localidad;
			$this->telefono_principal = $telefono_principal;
			$this->telefono_secundario = $telefono_secundario;
			$this->email = $email;
			$this->cargo = $cargo;
			$this->estado = $estado;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdEmpleado(){
			return $this->id_empleado;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		function getDocumento(){
			return $this->documento;
		}
		
		function getDireccion(){
			return $this->direccion;
		}
		
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getTelefonoPrincipal(){
			return $this->telefono_principal;
		}
			
		function getTelefonoSecundario(){
			return $this->telefono_secundario;
		}
				
		function getEmail(){
			return $this->email;
		}	
		function getCargo(){
			return $this->cargo;
		}
		
		function getEstado(){
			return $this->estado;
		}
		
		// Métodos Setters
		function setIdEmpleado($id_empleado){
			$this->id_empleado = $id_empleado;
		}
		
		function setNombre($nombre){
			$this->nombre = $nombre;
		}	
		function setDocumento($documento){
			$this->documento = $documento;
		}
		
		function setDireccion($direccion){
			$this->direccion = $direccion;
		}
		
		function setLocalidad($localidad){
			$this->localidad = $localidad;
		}
		
		function setTelefonoPrincipal($telefono_principal){
			$this->telefono_principal = $telefono_principal;
		}
				
		function setTelefonoSecundario($telefono_secundario){
			$this->telefono_secundario = $telefono_secundario;
		}	
		
		function setEmail($email){
			$this->email = $email;
		}
		function setCargo($cargo){
			$this->cargo = $cargo;
		}
				
		function setEstado($estado){
			$this->estado = $estado;
		}
			
		// Trae el empleado
		function recuperarEmpleado($id){
			$sql = "select * from empleados where id_empleado = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_empleado = $fila[0]["id_empleado"];
				$this->nombre = $fila[0]["nombre"];	
				$this->documento = $fila[0]["documento"];
				$this->direccion = $fila[0]["direccion"];
				$this->localidad = $fila[0]["localidad"];
				$this->telefono_principal = $fila[0]["telefono_principal"];
				$this->telefono_secundario = $fila[0]["telefono_secundario"];
				$this->email = $fila[0]["email"];
				$this->cargo = $fila[0]["cargo"];
				$this->estado = $fila[0]["estado"];
			}
		}
		
		// Lista todos los empleadoes
		function listarEmpleados(){
			$sql = "select id_empleado, nombre, documento, direccion, cargo, estado
			from empleados order by nombre";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarEmpleado(){
			$sql = "insert into empleados(nombre,documento,direccion,
			localidad,telefono_principal,telefono_secundario,
			email,cargo,estado)
			values('$this->nombre',
			'$this->documento',
			'$this->direccion',
			'$this->localidad',
			'$this->telefono_principal',
			'$this->telefono_secundario',
			'$this->email',
			'$this->cargo',
			'$this->estado')";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarEmpleado(){
			$sql = "update empleados
			set nombre = '$this->nombre',
			documento = '$this->documento',
			direccion = '$this->direccion',
			localidad = '$this->localidad',
			telefono_principal = '$this->telefono_principal',
			telefono_secundario = '$this->telefono_secundario',
			email = '$this->email',
			cargo = '$this->cargo',
			estado = '$this->estado'
			where id_empleado = '$this->id_empleado'";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarEmpleado($id){
			$sql = "delete from empleados where id_empleado = '" . $id . "'";
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>