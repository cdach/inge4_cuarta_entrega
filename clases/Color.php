<?php
	include_once("Conexion.php");
	
	class Color{
		private $id_color;
		private $nombre;
		private $codigo;
		
		
		function __construct($id_color="",$nombre="",$codigo=""){
			$this->id_modelo = $id_color;
			$this->nombre = $nombre;
			$this->codigo = $codigo;
			$this->conexion = new Conexion();
		}
			
		function __destruct(){
			$this->conexion = null;
		}
			
		// Métodos Getters
		function getIdColor(){
			return $this->id_color;
		}
		function getNombre(){
			return $this->nombre;
		}	
		function getCodigo(){
			return $this->codigo;
		}
		
		function setIdColor($id_color){
			$this->id_color = $id_color;
		}
		function setNombre($nombre){
			$this->nombre = $nombre;
		}	
		function setCodigo($codigo){
			$this->codigo = $codigo;
		}
		
		function recuperarColor($id){
			$sql = "select * from vehiculos_colores where id_color = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_color = $fila[0]["id_color"];
				$this->nombre = $fila[0]["nombre"];
				$this->codigo = $fila[0]["codigo"];
			}
		}
		
		function listarColores(){
            $sql = "select     id_color,
					nombre,
					codigo
					from  vehiculos_colores";
            $rs = $this->conexion->consultarSql($sql);
            return $rs;
        }

		function grabarColor(){
			$sql = "insert into vehiculos_colores(nombre,codigo)
			values('$this->nombre','$this->codigo')";
			
			return $this->conexion->consultarSql($sql,false);
		}
			
		// Editar el registro
		function editarColor(){
			$sql = "update vehiculos_colores
			set nombre = '$this->nombre',
				codigo = '$this->codigo'
			where id_color = '$this->id_color'";
				
			return $this->conexion->consultarSql($sql,false);
		}
			
			// Borrar el registro
		function borrarColor($id){
			$sql = "delete from vehiculos_colores where id_color = '" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>