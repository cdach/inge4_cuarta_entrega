<?php
	include_once("Conexion.php");
	
	class VehiculoMarca{
		private $id_marca;
		private $nombre;
		
		
		function __construct($id_marca="",$nombre=""){
			$this->id_marca = $id_marca;
			$this->nombre = $nombre;
			$this->conexion = new Conexion();
		}
			
		function __destruct(){
			$this->conexion = null;
		}
			
		// Métodos Getters
		function getIdVehiculoMarca(){
			return $this->id_marca;
		}
			
		function getNombre(){
			return $this->nombre;
		}
		
		function setIdVehiculoMarca($id_marca){
			$this->id_marca = $id_marca;
		}
			
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function recuperarVehiculoMarca($id){
			$sql = "select * from vehiculos_marcas where id_marca = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_marca = $fila[0]["id_marca"];
				$this->nombre = $fila[0]["nombre"];
			}
		}
		
		function listarVehiculosMarcas(){
			$sql = "select id_marca, nombre from vehiculos_marcas order by nombre";
				
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}

		function grabarVehiculoMarca(){
			$sql = "insert into vehiculos_marcas(nombre)
			values('$this->nombre')";
			
			return $this->conexion->consultarSql($sql,false);
		}
			
		// Editar el registro
		function editarVehiculoMarca(){
			$sql = "update vehiculos_marcas
			set nombre = '$this->nombre'
			where id_marca = '$this->id_marca'";
				
			return $this->conexion->consultarSql($sql,false);
		}
			
			// Borrar el registro
		function borrarVehiculoMarca($id){
			$sql = "delete from vehiculos_marcas where id_marca = '" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>