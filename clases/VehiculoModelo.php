<?php
	include_once("Conexion.php");
	
	class VehiculoModelo{
		private $id_modelo;
		private $id_marca;
		private $nombre;
		
		
		function __construct($id_modelo="",$id_marca="",$nombre=""){
			$this->id_modelo = $id_modelo;
			$this->id_marca = $id_marca;
			$this->nombre = $nombre;
			$this->conexion = new Conexion();
		}
			
		function __destruct(){
			$this->conexion = null;
		}
			
		// Métodos Getters
		function getIdVehiculoModelo(){
			return $this->id_modelo;
		}
		function getIdVehiculoMarca(){
			return $this->id_marca;
		}	
		function getNombre(){
			return $this->nombre;
		}
		
		function setIdVehiculoModelo($id_modelo){
			$this->id_modelo = $id_modelo;
		}
		function setIdVehiculoMarca($id_marca){
			$this->id_marca = $id_marca;
		}	
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function recuperarVehiculoModelo($id){
			$sql = "select * from vehiculos_modelos where id_modelo = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_modelo = $fila[0]["id_modelo"];
				$this->id_marca = $fila[0]["id_marca"];
				$this->nombre = $fila[0]["nombre"];
			}
		}
		
		function listarVehiculosModelos(){
            $sql = "select     vmo.id_modelo,
					vmo.nombre,
					vma.nombre as nombre_marca
					from  vehiculos_modelos as vmo, vehiculos_marcas as vma
					where vmo.id_marca = vma.id_marca";
            $rs = $this->conexion->consultarSql($sql);
            return $rs;
        }

		function grabarVehiculoModelo(){
			$sql = "insert into vehiculos_modelos(id_marca,nombre)
			values('$this->id_marca','$this->nombre')";
			
			return $this->conexion->consultarSql($sql,false);
		}
			
		// Editar el registro
		function editarVehiculoModelo(){
			$sql = "update vehiculos_modelos
			set id_marca = '$this->id_marca',
				nombre = '$this->nombre'
			where id_modelo = '$this->id_modelo'";
				
			return $this->conexion->consultarSql($sql,false);
		}
			
			// Borrar el registro
		function borrarVehiculoModelo($id){
			$sql = "delete from vehiculos_modelos where id_modelo = '" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>