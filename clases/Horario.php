<?php
	include_once("Conexion.php");
	
	class Horario{
		private $id_horario;
		private $id_taller;
		private $dia;
		private $desde;
		private $hasta;
		private $conexion;

		// Constructor
		function __construct($id_horario="",$id_taller="",$dia="",$desde="",$hasta=""){
			$this->id_horario = $id_horario;
			$this->id_taller = $id_taller;
			$this->dia = $dia;
			$this->desde = $desde;
			$this->hasta = $hasta;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdHorario(){
			return $this->id_horario;
		}
		
		function getIdTaller(){
			return $this->id_taller;
		}
		
		function getDia(){
			return $this->dia;
		}
		
		function getDesde(){
			return $this->desde;
		}
		
		function getHasta(){
			return $this->hasta;
		}
		
		// Métodos Setters
		function setIdHorario($id_horario){
			$this->id_horario = $id_horario;
		}
		
		function setIdTaller($id_taller){
			$this->id_taller = $id_taller;
		}
		
		function setDia($dia){
			$this->dia = $dia;
		}
		
		function setDesde($desde){
			$this->desde = $desde;
		}
		
		function setHasta($hasta){
			$this->hasta = $hasta;
		}
			
		// Trae el horario
		function recuperarHorario($id){
			$sql = "select * from horarios where id_horario = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_horario = $fila[0]["id_horario"];
				$this->id_taller = $fila[0]["id_taller"];	
				$this->dia = $fila[0]["dia"];
				$this->desde = $fila[0]["desde"];
				$this->hasta = $fila[0]["hasta"];
			}
		}
		
		// Lista todos los horarios
		function listarHorarios(){
			$sql = "select horarios.id_horario,
			talleres.nombre,
			(case when dia = '1' then 'Domingo' 
			 when dia = '2' then 'Lunes'
			 when dia = '3' then 'Martes'
			 when dia = '4' then 'Miércoles'
			 when dia = '5' then 'Jueves'
			 when dia = '6' then 'Viernes'
			 when dia = '7' then 'Sábado'
			end) as nombre_dia,
			horarios.desde,
			horarios.hasta
			from horarios, talleres
			where horarios.id_taller = talleres.id_taller order by dia, desde, hasta";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarHorario(){
			$sql = "insert into horarios(id_taller, dia, desde, hasta)
			values('$this->id_taller',
			'$this->dia',
			'$this->desde',
			'$this->hasta')";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarHorario(){
			$sql = "update horarios
			set id_taller = '$this->id_taller',
			dia = '$this->dia',
			desde = '$this->desde',
			hasta = '$this->hasta'
			where id_horario = '$this->id_horario'";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarHorario($id){
			$sql = "delete from horarios where id_horario = '" . $id . "'";
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>