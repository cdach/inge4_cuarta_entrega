<?php
	include_once("Conexion.php");
	
	class Cliente{
		private $id_cliente;
		private $nombre;
		private $documento;
		private $digito;
		private $direccion;
		private $localidad;
		private $telefono_principal;
		private $telefono_secundario;
		private $email;
		private $estado;
		private $conexion;

		// Constructor
		function __construct($id_cliente="",$nombre="",$documento="",$digito="",$direccion="",$localidad="",
			$telefono_principal="",$telefono_secundario="",$email="",$estado=""){
			$this->id_cliente = $id_cliente;
			$this->nombre = $nombre;
			$this->documento = $documento;
			$this->digito = $digito;
			$this->direccion = $direccion;
			$this->localidad = $localidad;
			$this->telefono_principal = $telefono_principal;
			$this->telefono_secundario = $telefono_secundario;
			$this->email = $email;
			$this->estado = $estado;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdCliente(){
			return $this->id_cliente;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getDocumento(){
			return $this->documento;
		}
		
		function getDigito(){
			return $this->digito;
		}
		
		function getDireccion(){
			return $this->direccion;
		}
		
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getTelefonoPrincipal(){
			return $this->telefono_principal;
		}
			
		function getTelefonoSecundario(){
			return $this->telefono_secundario;
		}
				
		function getEmail(){
			return $this->email;
		}	
		function getEstado(){
			return $this->estado;
		}
		
		// Métodos Setters
		function setIdCliente($id_cliente){
			$this->id_cliente = $id_cliente;
		}
		
		function setNombre($nombre){
			$this->nombre = $nombre;
		}	
		function setDocumento($documento){
			$this->documento = $documento;
		}
		function setDigito($digito){
			$this->digito = $digito;
		}
		function setDireccion($direccion){
			$this->direccion = $direccion;
		}
		
		function setLocalidad($localidad){
			$this->localidad = $localidad;
		}
		
		function setTelefonoPrincipal($telefono_principal){
			$this->telefono_principal = $telefono_principal;
		}
				
		function setTelefonoSecundario($telefono_secundario){
			$this->telefono_secundario = $telefono_secundario;
		}	
		
		function setEmail($email){
			$this->email = $email;
		}	
		function setEstado($estado){
			$this->estado = $estado;
		}
			
		// Trae el cliente
		function recuperarCliente($id){
			$sql = "select * from clientes where id_cliente = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_cliente = $fila[0]["id_cliente"];
				$this->nombre = $fila[0]["nombre"];	
				$this->documento = $fila[0]["documento"];
				$this->digito = $fila[0]["digito"];
				$this->direccion = $fila[0]["direccion"];
				$this->localidad = $fila[0]["localidad"];
				$this->telefono_principal = $fila[0]["telefono_principal"];
				$this->telefono_secundario = $fila[0]["telefono_secundario"];
				$this->email = $fila[0]["email"];
				$this->estado = $fila[0]["estado"];
			}
		}
		
		// Lista todos los clientes
		function listarClientes(){
			$sql = "select id_cliente, nombre, documento,digito, direccion, estado
			from clientes order by nombre";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarCliente(){
			$sql = "insert into clientes(nombre,documento,digito,direccion,
			localidad,telefono_principal,telefono_secundario,
			email,estado)
			values('$this->nombre',
			'$this->documento',
			'$this->digito',
			'$this->direccion',
			'$this->localidad',
			'$this->telefono_principal',
			'$this->telefono_secundario',
			'$this->email',
			'$this->estado')";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarCliente(){
			$sql = "update clientes
			set nombre = '$this->nombre',
			documento = '$this->documento',
			digito = '$this->digito',
			direccion = '$this->direccion',
			localidad = '$this->localidad',
			telefono_principal = '$this->telefono_principal',
			telefono_secundario = '$this->telefono_secundario',
			email = '$this->email',
			estado = '$this->estado'
			where id_cliente = '$this->id_cliente'";
				
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarCliente($id){
			$sql = "delete from clientes where id_cliente = '" . $id . "'";
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>